// window.$ = $;
// window.jQuery = $;
//
// require('OwlCarousel2-2.3.4/dist/owl.carousel.min.js');
//

// Enable Javascript & Jquery
$(document).foundation();
// Enable Ajax calls
$(document).ajaxComplete(function(){ $(document).foundation(); });

//  Uncheck checkboxes on page reload.
$(':checkbox:checked').prop('checked',false);

// Refresh the parallax effect
jQuery(window).trigger('resize').trigger('scroll');

//  Return to top
$(window).scroll(function() {
    if ($(this).scrollTop() >= 750) {
        $('#return-to-top').fadeIn(300);
    } else {
        $('#return-to-top').fadeOut(300);
    }
});
$('#return-to-top').click(function() {
    $('body,html').animate({
        scrollTop : 0
    }, 500);
});


//  Dropdown menu
(function ($) {
    $.fn.menuAim = function (opts) {
        // Initialize menu-aim for all elements in jQuery collection
        this.each(function () {
            init.call(this, opts);
        });
        return this;
    };

    function init(opts) {
        let $menu = $(this),
            activeRow = null,
            mouseLocs = [],
            lastDelayLoc = null,
            timeoutId = null,
            options = $.extend({
                rowSelector: "> li",
                submenuSelector: "*",
                submenuDirection: "right",
                tolerance: 75,  // bigger = more forgivey when entering submenu
                enter: $.noop,
                exit: $.noop,
                activate: $.noop,
                deactivate: $.noop,
                exitMenu: $.noop
            }, opts);

        let MOUSE_LOCS_TRACKED = 3,  // number of past mouse locations to track
            DELAY = 300;  // ms delay when user appears to be entering submenu

        let mousemoveDocument = function (e) {
            mouseLocs.push({x: e.pageX, y: e.pageY});

            if (mouseLocs.length > MOUSE_LOCS_TRACKED) {
                mouseLocs.shift();
            }
        };

        let mouseleaveMenu = function () {
            if (timeoutId) {
                clearTimeout(timeoutId);
            }

            if (options.exitMenu(this)) {
                if (activeRow) {
                    options.deactivate(activeRow);
                }

                activeRow = null;
            }
        };

        let mouseenterRow = function () {
                if (timeoutId) {
                    clearTimeout(timeoutId); }
                options.enter(this);
                possiblyActivate(this); },
                mouseleaveRow = function () {
                options.exit(this); };

        let clickRow = function () {
            activate(this); };

        let activate = function (row) {
            if (row === activeRow) {
                return;
            } if (activeRow) {
                options.deactivate(activeRow); }
            options.activate(row);
            activeRow = row;
        };

        let possiblyActivate = function (row) {
            let delay = activationDelay();
            if (delay) {
                timeoutId = setTimeout(function () {
                    possiblyActivate(row);
                }, delay);
            } else {
                activate(row);
            }
        };

        let activationDelay = function () {
            if (!activeRow || !$(activeRow).is(options.submenuSelector)) {
                // If there is no other submenu row already active, then
                // go ahead and activate immediately.
                return 0;
            }

            let offset = $menu.offset(),
                upperLeft = {
                    x: offset.left,
                    y: offset.top - options.tolerance
                },
                upperRight = {
                    x: offset.left + $menu.outerWidth(),
                    y: upperLeft.y
                },
                lowerLeft = {
                    x: offset.left,
                    y: offset.top + $menu.outerHeight() + options.tolerance
                },
                lowerRight = {
                    x: offset.left + $menu.outerWidth(),
                    y: lowerLeft.y
                },
                loc = mouseLocs[mouseLocs.length - 1],
                prevLoc = mouseLocs[0];

            if (!loc) {
                return 0;
            }

            if (!prevLoc) {
                prevLoc = loc;
            }

            if (prevLoc.x < offset.left || prevLoc.x > lowerRight.x ||
                prevLoc.y < offset.top || prevLoc.y > lowerRight.y) {
                return 0;
            }

            if (lastDelayLoc &&
                loc.x == lastDelayLoc.x && loc.y == lastDelayLoc.y) {
                return 0;
            }

            function slope(a, b) {
                return (b.y - a.y) / (b.x - a.x);
            }

            let decreasingCorner = upperRight,
                increasingCorner = lowerRight;

            if (options.submenuDirection == "left") {
                decreasingCorner = lowerLeft;
                increasingCorner = upperLeft;
            } else if (options.submenuDirection == "below") {
                decreasingCorner = lowerRight;
                increasingCorner = lowerLeft;
            } else if (options.submenuDirection == "above") {
                decreasingCorner = upperLeft;
                increasingCorner = upperRight;
            }

            let decreasingSlope = slope(loc, decreasingCorner),
                increasingSlope = slope(loc, increasingCorner),
                prevDecreasingSlope = slope(prevLoc, decreasingCorner),
                prevIncreasingSlope = slope(prevLoc, increasingCorner);

            if (decreasingSlope < prevDecreasingSlope &&
                increasingSlope > prevIncreasingSlope) {
                lastDelayLoc = loc;
                return DELAY;
            }

            lastDelayLoc = null;
            return 0;
        };

        $menu
            .mouseleave(mouseleaveMenu)
            .find(options.rowSelector)
            .mouseenter(mouseenterRow)
            .mouseleave(mouseleaveRow)
            .click(clickRow);

        $(document).mousemove(mousemoveDocument);

    }
})(jQuery);

jQuery(document).ready(function ($) {
    //open/close mega-navigation
    $('.cd-dropdown-trigger').on('click', function (event) {
        event.preventDefault();
        toggleNav();
    });

    //close meganavigation
    $('.cd-dropdown .cd-close').on('click', function (event) {
        event.preventDefault();
        toggleNav();
    });

    //on mobile - open submenu
    $('.has-children').children('a').on('click', function (event) {
        //prevent default clicking on direct children of .has-children
        event.preventDefault();
        let selected = $(this);
        selected.next('ul').removeClass('is-hidden').end().parent('.has-children').parent('ul').addClass('move-out');
    });

    //on desktop - differentiate between a user trying to hover over a dropdown item vs trying to navigate into a submenu's contents
    let submenuDirection = (!$('.cd-dropdown-wrapper').hasClass('open-to-left')) ? 'right' : 'left';
    $('.cd-dropdown-content').menuAim({
        activate: function (row) {
            $(row).children().addClass('is-active').removeClass('fade-out');
            if ($('.cd-dropdown-content .fade-in').length == 0) $(row).children('ul').addClass('fade-in');
        },
        deactivate: function (row) {
            $(row).children().removeClass('is-active');
            if ($('li.has-children:hover').length == 0 || $('li.has-children:hover').is($(row))) {
                $('.cd-dropdown-content').find('.fade-in').removeClass('fade-in');
                $(row).children('ul').addClass('fade-out')
            }
        },
        exitMenu: function () {
            $('.cd-dropdown-content').find('.is-active').removeClass('is-active');
            return true;
        },
        submenuDirection: submenuDirection,
    });

    //submenu items - go back link
    $('.go-back').on('click', function () {
        let selected = $(this),
            visibleNav = $(this).parent('ul').parent('.has-children').parent('ul');
        selected.parent('ul').addClass('is-hidden').parent('.has-children').parent('ul').removeClass('move-out');
    });

    function toggleNav() {

        var $cdDropdown = $('.cd-dropdown');
        let navIsVisible = (!$cdDropdown.hasClass('dropdown-is-active'));
        $cdDropdown.toggleClass('dropdown-is-active', navIsVisible);
        $('.cd-dropdown-trigger').toggleClass('dropdown-is-active', navIsVisible);
        if (!navIsVisible) {
            $($cdDropdown).one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function () {
                $('.has-children ul').addClass('is-hidden');
                $('.move-out').removeClass('move-out');
                $('.is-active').removeClass('is-active');
            });
        }
    }
});






// AJAX LOADING BAR
// ________________
jQuery(function($) {

    var $loadingSelector = $('#loading-bar');

    $(document).ajaxStart(function() {
        if ($loadingSelector.length === 0) {
            $('body').append( $('<div/>').attr('id', 'loading-bar').addClass(_lb.position) );
            _lb.percentage = Math.random() * 30 + 30;
            $("#loading-bar")[_lb.direction](_lb.percentage + "%");
            _lb.interval = setInterval(function() {
                _lb.percentage = Math.random() * ( (100-_lb.percentage) / 2 ) + _lb.percentage;
                $("#loading-bar")[_lb.direction](_lb.percentage + "%");
            }, 500);
        }

    }).ajaxStop(function() {
        clearInterval(_lb.interval);
        $("#loading-bar")[_lb.direction]("101%");

        // Waits until css transition is finished and removes element from the DOM
        setTimeout( function() {
            $("#loading-bar").fadeOut(300, function() {
                $(this).remove();
            });
        }, 300);

    });
});

// Main object
var _lb = {};

// Default loading bar position
_lb.position  = 'top';
_lb.direction = 'width';

// Ajax call
// Accepts callback( response )
_lb.get = function( callback ) {
    _lb.loading = true;
    jQuery.ajax({
        url   : this.href,
        success: function(response) {
            _lb.loading = false;
            if ( typeof(callback) === 'function'  )
                callback( response );
        }
    });
};

jQuery(function($) {
    $('.btn-action').click(function() {
        switch ( $(this).data('action') ) {
            case 'load':
                _lb.get();
                break;
            case 'position':
                _lb.position  = $(this).data('position');
                _lb.direction = $(this).data('direction');
                $('#section-position h1 small').html( _lb.position );
                break;
        }
    });
});






// AJAX FUNCTIONS
// ___________________________

$('#back_to_top').click(function(){
    if($(this).is(':checked')) {
        $("#back_to_top-container").load("components/atoms/backToTop.html");
    } else {
        $("#back_to_top-container").empty();
    }
});

$('#login').click(function(){
    if($(this).is(':checked')) {
        // console.log('Checked');
        $("#login-container").load("components/-foundation/login.php");
    } else {
        // console.log('Unchecked');
        $("#login-container").empty();
    }
});

$('#register').click(function(){
    if($(this).is(':checked')) {
        console.log("Checked");
        $("#register-container").load("components/-foundation/register.php");
    } else {
        console.log("unchecked");
        $("#register-container").empty();
    }
});

$('#pagination').click(function(){
    if($(this).is(':checked')) {
        $("#pagination-container").load("components/-foundation/pagination.php");
    } else {
        $("#pagination-container").empty();
    }
});


$('#breadcrumbs').click(function(){
    if($(this).is(':checked')) {
        $("#breadcrumbs-container").load("components/-foundation/breadcrumbs.php");
    } else {
        $("#breadcrumbs-container").empty();
    }
});


$('#megellan').click(function(){
    if($(this).is(':checked')) {
        $("#magellan-container").load("components/-foundation/magellan.php");
    } else {
        $("#magellan-container").empty();
    }
});

$('#accordion').click(function(){
    if($(this).is(':checked')) {
        $("#accordion-container").load("components/-foundation/accordion.php");
    } else {
        $("#accordion-container").empty();
    }
});

$('#responsive-a').click(function(){
    if($(this).is(':checked')) {
        $("#responsive-accordion").load("components/-foundation/responsive-accordion.php");
    } else {
        $("#responsive-accordion").empty();
    }
});

$('#magellan').click(function(){
    if($(this).is(':checked')) {
        console.log("checked");
        $("#magellan-container").load("components/-foundation/magellan.php");
    } else {
        console.log("unchecked");
        $("#magellan-container").empty();
    }
});

$('#callout').click(function(){
    if($(this).is(':checked')) {
        $("#callout-container").load("components/-foundation/callout.php");
    } else {
        $("#callout-container").empty();
    }
});


$('#dropdown').click(function(){
    if($(this).is(':checked')) {
        $("#dropdown-container").load("components/-foundation/dropdown.php");
    } else {
        $("#dropdown-container").empty();
    }
});

$('#media-object').click(function(){
    if($(this).is(':checked')) {
        $("#media-object-container").load("components/-foundation/media-object.php");
    } else {
        $("#media-object-container").empty();
    }
});

$('#off-canvas').click(function(){
    if($(this).is(':checked')) {
        $("#off-canvas-container").load("components/-foundation/off-canvas.php");
    } else {
        $("#off-canvas-container").empty();
    }
});

$('#reveal').click(function(){
    if($(this).is(':checked')) {
        $("#reveal-container").load("components/-foundation/reveal.php");
    } else {
        $("#reveal-container").empty();
    }
});

$('#table').click(function(){
    if($(this).is(':checked')) {
        $("#table-container").load("components/-foundation/table.php");
    } else {
        $("#table-container").empty();
    }
});

$('#tabs').click(function(){
    if($(this).is(':checked')) {
        $("#tabs-container").load("components/-foundation/tabs.php");
    } else {
        $("#tabs-container").empty();
    }
});

$('#orbit').click(function(){
    if($(this).is(':checked')) {

        $("#orbit-container").load("components/-foundation/orbit.php");


    } else {
        $("#orbit-container").empty();
    }
});

$('#progress-b').click(function(){
    if($(this).is(':checked')) {
        $("#progress-container").load("components/-foundation/progress-bar.php");
    } else {
        $("#progress-container").empty();
    }
});

$('#thumbnail').click(function(){
    if($(this).is(':checked')){
        $('#thumbnail-container').load("components/molecules/thumbnail.php");
    } else {
        $("#thumbnail-container").empty();
    }
});

$('#card').click(function(){
    if($(this).is(':checked')){
        $('#card-container').load("components/-foundation/card.php");

    } else {
        $("#card-container").empty();
    }

});


// Atoms
$('#badge').click(function(){
    if($(this).is(':checked')){
        $('#badge-container').load("components/-foundation/badge.php");
    } else {
        $("#badge-container").empty();
    }
});

$('#buttons').click(function(){
    if($(this).is(':checked')){
        $('#buttons-container').load("components/-foundation/buttons.php");
    } else {
        $("#buttons-container").empty();
    }
});

$('#close-button').click(function(){
    if($(this).is(':checked')){
        $('#close-button-container').load("components/-foundation/close-button.php");
    } else {
        $("#close-button-container").empty();
    }
});

$('#form').click(function(){
    if($(this).is(':checked')){
        $('#form-container').load("components/-foundation/form.php");
    } else {
        $("#form-container").empty();
    }
});

$('#switch').click(function(){
    if($(this).is(':checked')){
        $('#switch-container').load("components/-foundation/switch.php");
    } else {
        $("#switch-container").empty();
    }
});

$('#slider').click(function(){
    if($(this).is(':checked')){
        $('#slider-container').load("components/-foundation/slider.php");
    } else {
        $("#slider-container").empty();
    }
});


$('#tooltip').click(function(){
    if($(this).is(':checked')){
        $("#tooltip-container").load("components/-foundation/tooltip.php");
    } else {
        $("#tooltip-container").empty();
    }
});

$('#badges').click(function(){
    if($(this).is(':checked')){
        $("#badges-container").load("components/-foundation/badge.php");
    } else {
        $("#badges-container").empty();
    }
});

$('#welcome').click(function(){
    if($(this).is(':checked')){
        $("#welcome-container").load("components/-base/welcome.html");
    } else {
        $("#welcome-container").empty();
    }
});


// Listen for click on toggle checkbox
$('#select-all').click(function() {
    if(this.checked) {
        // Doorloop elke checkbox
        $(':checkbox').each(function() {
            (this).checked = true;
            $("#accordion-container").load("components/1-molecules/accordion.php");
            $("#breadcrumbs-container").load("components/1-molecules/breadcrumbs.php");
            $('#slider-container').load("components/1-molecules/slider.php");
            $('#switch-container').load("components/0-atoms/switch.php");
            $('#form-container').load("components/0-atoms/form.php");
            $('#close-button-container').load("components/0-atoms/close-button.php");
            $('#buttons-container').load("components/0-atoms/buttons.php");
            $('#badge-container').load("components/0-atoms/badge.php");
            $('#card-container').load("components/1-molecules/card.php");
            $('#thumbnail-container').load("components/1-molecules/thumbnail.php");
            $("#progress-container").load("components/1-molecules/progress-bar.php");
            $("#orbit-container").load("components/1-molecules/orbit.php");
            $("#tabs-container").load("components/1-molecules/tabs.php");
            $("#table-container").load("components/1-molecules/table.php");
            $("#reveal-container").load("components/1-molecules/reveal.php");
            $("#media-object-container").load("components/1-molecules/media-object.php");
            $("#dropdown-container").load("components/1-molecules/dropdown.php");
            $("#callout-container").load("components/1-molecules/callout.php");
            $("#magellan-container").load("components/1-molecules/magellan.php");
            $("#responsive-accordion").load("components/1-molecules/responsive-accordion.php");
            $("#login-container").load("components/1-molecules/login.php");
            $("#register-container").load("components/1-molecules/register.php");
            $("#pagination-container").load("components/1-molecules/pagination.php");
            $("#tooltip-container").load("components/0-atoms/tooltip.php");
            $("#badges-container").load("components/0-atoms/badges.php");
        });
    } else {
        $(':checkbox').each(function() {
            (this).checked = false;
            $("#breadcrumbs-container").empty();
            $("#slider-container").empty();
            $("#switch-container").empty();
            $("#form-container").empty();
            $("#close-button-container").empty();
            $("#buttons-container").empty();
            $("#badge-container").empty();
            $("#card-container").empty();
            $("#thumbnail-container").empty();
            $("#progress-container").empty();
            $("#orbit-container").empty();
            $("#tabs-container").empty();
            $("#table-container").empty();
            $("#reveal-container").empty();
            $("#media-object-container").empty();
            $("#dropdown-container").empty();
            $("#callout-container").empty();
            $("#responsive-accordion").empty();
            $("#register-container").empty();
            $("#pagination-container").empty();
            $("#login-container").empty();
            $("#magellan-container").empty();
            $("#tooltip-container").empty();
            $("#badges-container").empty();
        });
    }
});



// FUNCTION - COPY TO CLIPBOARD

// Copy text inside the container with a id of to-copy
function CopyToClipboard (containerid) {
    var textarea = document.createElement('textarea');
    textarea.id = 'temp_element';
    textarea.style.height = 0;
    document.body.appendChild(textarea);
    textarea.value = document.getElementById(containerid).innerText;
    var selector = document.querySelector('#temp_element');
    selector.select();
    document.execCommand('copy');
    document.body.removeChild(textarea)
}

function CallCopyToClipboard(){
    document.getElementById('copied').addEventListener('click', function (clicked) {
        return function () {
            if (!clicked) {
                var last = this.innerHTML;
                this.innerHTML = '<i class="far fa-copy hide-fa"><span>Copied!</span></i>';
                clicked = true;
                setTimeout(function () {
                    this.innerHTML = last;
                    clicked = false;
                }.bind(this), 2000);
            }
        };
    }(false))
}
























