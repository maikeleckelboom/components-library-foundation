<?php include 'components/-base/head.html' ?>




<div class="off-canvas position-left" id="offCanvas" data-off-canvas>
    <!-- Your menu or Off-canvas content goes here -->
    <ul class="menu vertical drilldown" data-drilldown data-auto-height="true" data-animate-height="true" style="max-width: 250px" id="m3">
        <li>
            <a href="#">Item 1</a>
            <ul class="menu">
                <li>
                    <a href="#">Item 1A</a>
                    <ul class="menu">
                        <li><a href="#Item-1Aa">Item 1Aa</a></li>
                        <li><a href="#Item-1Ba">Item 1Ba</a></li>
                    </ul>
                </li>
                <li><a href="#Item-1B">Item 1B</a></li>
                <li><a href="#Item-1C">Item 1C</a></li>
                <li><a href="#Item-1D">Item 1D</a></li>
                <li><a href="#Item-1E">Item 1E</a></li>
            </ul>
        </li>
        <li>
            <a href="#">Item 2</a>
            <ul class="menu">
                <li><a href="#Item-2A">Item 2A</a></li>
                <li><a href="#Item-2B">Item 2B</a></li>
                <li><a href="#Item-2C">Item 2C</a></li>
                <li><a href="#Item-2D">Item 2D</a></li>
                <li><a href="#Item-2E">Item 2E</a></li>
            </ul>
        </li>
        <li>
            <a href="#">Item 3</a>
            <ul class="menu">
                <li><a href="#Item-3A">Item 3A</a></li>
                <li><a href="#Item-3B">Item 3B</a></li>
                <li><a href="#Item-3C">Item 3C</a></li>
                <li><a href="#Item-3D">Item 3D</a></li>
                <li>
                    <a href="#Item-3E">Item 3E</a>
                    <ul class="menu">
                        <li><a href="#Item-3EA">Item 3EA</a></li>
                        <li><a href="#Item-3EB">Item 3EB</a></li>
                        <li><a href="#Item-3EC">Item 3EC</a></li>
                        <li><a href="#Item-3ED">Item 3ED</a></li>
                        <li><a href="#Item-3EE">Item 3EE</a></li>
                        <li><a href="#Item-3EA">Item 3EA</a></li>
                        <li><a href="#Item-3EB">Item 3EB</a></li>
                        <li><a href="#Item-3EC">Item 3EC</a></li>
                        <li><a href="#Item-3ED">Item 3ED</a></li>
                        <li><a href="#Item-3EE">Item 3EE</a></li>
                        <li><a href="#Item-3EA">Item 3EA</a></li>
                        <li><a href="#Item-3EB">Item 3EB</a></li>
                        <li><a href="#Item-3EC">Item 3EC</a></li>
                        <li><a href="#Item-3ED">Item 3ED</a></li>
                        <li><a href="#Item-3EE">Item 3EE</a></li>
                    </ul>
                </li>
            </ul>
        </li>
        <li><a href="#Item-4"> Item 4</a></li>
        <li><a href="#Item-5"> Item 5</a></li>
        <li><a href="#Item-6"> Item 6</a></li>
        <li><a href="#Item-7"> Item 7</a></li>
        <li><a href="#Item-8"> Item 8</a></li>
    </ul>





    <button class="close-button" aria-label="Close menu" type="button" data-close>
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="off-canvas-content" data-off-canvas-content>
    <!-- Your page content lives here -->

    
</div>


<script>
    // Enable Javascript & Jquery
    $(document).foundation();

    $('.new-back').each(function(){
        var backTxt = $(this).parent().closest('.is-drilldown-submenu-parent').find('> a').text();
        $(this).text(backTxt);
    });
</script>

<?php include 'components/-base/footer.html' ?>
