<?php include 'components/-base/head.html' ?>

    <script src="//cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.4.0/clipboard.min.js"></script>




    <pre>
    <code class="language-less">@bg: transparent;
    .element {
        &amp; when not (@bg = transparent) {
            background: @bg;
        }
    }</code>
    </pre>


    <script>

        (function(){
            new Clipboard('#copy-button');
            var pre = document.getElementsByTagName('pre');

            for (var i = 0; i < pre.length; i++) {
                var isLanguage = pre[i].children[0].className.indexOf('language-');
                if ( isLanguage === 0 ) {
                    var button           = document.createElement('button');
                    button.className = 'copy-button';
                    button.textContent = 'Copy';

                    pre[i].appendChild(button);
                }

                var copyCode = new Clipboard('.copy-button', {
                    target: function(trigger) {
                        return trigger.previousElementSibling;
                    }
                });
            }

            copyCode.on('success', function(event) {
                event.clearSelection();
                event.trigger.textContent = 'Copied';
                window.setTimeout(function() {
                    event.trigger.textContent = 'Copy';
                }, 2000);
            });

            copyCode.on('error', function(event) {
                event.trigger.textContent = 'Press "Ctrl + C" to copy';
                window.setTimeout(function() {
                    event.trigger.textContent = 'Copy';
                }, 2000);
            });

        })();

    </script>

<?php include 'components/-base/footer.html' ?>
