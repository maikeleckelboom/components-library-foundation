<div class="input-group search">
    <span class="input-group-label"><i class="fab fa-searchengin"></i></span>
    <input class="input-group-field" type="search" title="" placeholder="Search ..">
    <div class="input-group-button">
        <input type="submit" class="button adjust" value="Search">
    </div>
</div>