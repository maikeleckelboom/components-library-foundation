document.getElementById('copied').addEventListener('click', function (clicked) {
    return function () {
        if (!clicked) {
            var last = this.innerHTML;
            this.innerHTML = 'Copied!';
            clicked = true;
            setTimeout(function () {
                this.innerHTML = last;
                clicked = false;
            }.bind(this), 2000);
        }
    };
}(false))