<div class="grid-x master animated fadeIn">

    <div class="cell small-24 medium-11">
        <div class="cell-margin">
            <div class="card">
                <img src="images/dummy.jpg">
                <div class="card-section">
                    <p>This card makes use of the card-divider element.</p>
                </div>
                <div class="card-divider">
                    <h4>I'm featured</h4>
                </div>
            </div>
        </div>
    </div>


    <div class="cell small-24 medium-13 overflow">
        <div class="cell-margin">

            <h1 class="title">Card</h1>


            <div class="component-buttons">

                <button type="button" class="flip flip-card show-code"><i class="fas fa-code"></i> Code</button>
                <button type="button" onClick="CopyToClipboard('to-copy')" id="copied">
                    <i class="far fa-copy"></i> Copy</button>

            </div>


            <div class="panel panel-card" id="to-copy">
                <pre class="line-numbers language-markup">
                    <code id="to-copy">&lt;div class=&quot;card&quot;&gt;
                        &lt;img src=&quot;images/dummy.jpg&quot;&gt;
                        &lt;div class=&quot;card-section&quot;&gt;
                            &lt;p&gt;This card makes use of the card-divider element.&lt;/p&gt;
                        &lt;/div&gt;
                        &lt;div class=&quot;card-divider&quot;&gt;
                            &lt;h4&gt;I'm featured&lt;/h4&gt;
                        &lt;/div&gt;
                    &lt;/div&gt;</code>
                </pre>
            </div>

        </div>
    </div>
</div>


<script>
    Prism.highlightAll();
    CallCopyToClipboard();

    $(document).ready(function () {
        $(".flip-card").click(function () {
            $(".panel-card").slideToggle("slow");
        });
    });
</script>