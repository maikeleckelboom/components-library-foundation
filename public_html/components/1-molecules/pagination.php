<div class="grid-x master animated fadeIn">
    <div class="cell small-12">
        <div class="cell-margin">
            <nav aria-label="Pagination">
                <ul class="pagination">
                    <li class="pagination-previous disabled">Previous <span class="show-for-sr">page</span></li>
                    <li class="current"><span class="show-for-sr">You're on page</span> 1</li>
                    <li><a href="#" aria-label="Page 2">2</a></li>
                    <li><a href="#" aria-label="Page 3">3</a></li>
                    <li><a href="#" aria-label="Page 4">4</a></li>
                    <li class="ellipsis" aria-hidden="true"></li>
                    <li><a href="#" aria-label="Page 12">12</a></li>
                    <li><a href="#" aria-label="Page 13">13</a></li>
                    <li class="pagination-next"><a href="#" aria-label="Next page">Next <span class="show-for-sr">page</span></a></li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="cell small-12">
        <div class="cell-margin">

            <div class="button-group">
                <button class="flip button show-code flip-pagination">Show Code</button>
                <button onClick="CopyToClipboard('to-copy')" class="button" id="copied">
                    Copy to clipboard
                </button>
            </div>

            <div class="panel panel-pagination" id="to-copy">
        <pre class="line-numbers language-markup">
            <code id="to-copy">&lt;nav aria-label=&quot;Pagination&quot;&gt;
                &lt;ul class=&quot;pagination&quot;&gt;
                    &lt;li class=&quot;pagination-previous disabled&quot;&gt;Previous &lt;span class=&quot;show-for-sr&quot;&gt;page&lt;/span&gt;&lt;/li&gt;
                    &lt;li class=&quot;current&quot;&gt;&lt;span class=&quot;show-for-sr&quot;&gt;You're on page&lt;/span&gt; 1&lt;/li&gt;
                    &lt;li&gt;&lt;a href=&quot;#&quot; aria-label=&quot;Page 2&quot;&gt;2&lt;/a&gt;&lt;/li&gt;
                    &lt;li&gt;&lt;a href=&quot;#&quot; aria-label=&quot;Page 3&quot;&gt;3&lt;/a&gt;&lt;/li&gt;
                    &lt;li&gt;&lt;a href=&quot;#&quot; aria-label=&quot;Page 4&quot;&gt;4&lt;/a&gt;&lt;/li&gt;
                    &lt;li class=&quot;ellipsis&quot; aria-hidden=&quot;true&quot;&gt;&lt;/li&gt;
                    &lt;li&gt;&lt;a href=&quot;#&quot; aria-label=&quot;Page 12&quot;&gt;12&lt;/a&gt;&lt;/li&gt;
                    &lt;li&gt;&lt;a href=&quot;#&quot; aria-label=&quot;Page 13&quot;&gt;13&lt;/a&gt;&lt;/li&gt;
                    &lt;li class=&quot;pagination-next&quot;&gt;&lt;a href=&quot;#&quot; aria-label=&quot;Next page&quot;&gt;Next &lt;span class=&quot;show-for-sr&quot;&gt;page&lt;/span&gt;&lt;/a&gt;&lt;/li&gt;
                &lt;/ul&gt;
            &lt;/nav&gt;</code>
        </pre>
            </div>
        </div>
    </div>
</div>






<script>
    Prism.highlightAll();
    CallCopyToClipboard();

    $(document).ready(function () {
        $(".flip-pagination").click(function () {
            $(".panel-pagination").slideToggle("slow");
        });
    });
</script>
