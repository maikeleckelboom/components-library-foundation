
<section class="window">

<!--<p class="margin-left"><a href="https://foundation.zurb.com/sites/docs/off-canvas.html">Documentatie</a></p>-->

<button type="button" class="button" data-toggle="offCanvasLeftSplit1">Open Left</button>
<button type="button" class="button" data-toggle="offCanvasRightSplit2">Open Right</button>

<div class="grid-x grid-margin-x">
    <div class="cell small-12">
        <div class="off-canvas-wrapper">
            <div class="off-canvas-absolute position-left" id="offCanvasLeftSplit1" data-off-canvas>
                <!-- Your menu or Off-canvas content goes here -->
            </div>
            <div class="off-canvas-content" style="min-height: 300px;" data-off-canvas-content>
                <p>off-canvas on the right!</p>
            </div>
        </div>
    </div>

    <div class="cell small-12">
        <div class="off-canvas-wrapper">
            <div class="off-canvas-absolute position-right" id="offCanvasRightSplit2" data-off-canvas>
                <!-- Your menu or Off-canvas content goes here -->
            </div>
            <div class="off-canvas-content" style="min-height: 300px;" data-off-canvas-content>
                <p>Left!</p>
            </div>
        </div>
    </div>
</div>



</section>
