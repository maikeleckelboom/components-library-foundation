<div class="grid-x master animated fadeIn">

    <div class="cell small-24 medium-11 overflow">
        <div class="cell-margin">

            <div class="molecule-register">
                <div class="input-group">
                    <span class="input-group-label"><i class="fas fa-at"></i></span>
                    <input class="input-group-field" type="email" title="" placeholder="Email">
                </div>

                <div class="input-group">
                    <span class="input-group-label"><i class="fas fa-unlock"></i></span>
                    <input class="input-group-field" type="password" title="" placeholder="Wachtwoord">
                </div>

                <input type="button" class="button" value="Verzenden" name="">
            </div>

        </div>
    </div>

    <div class="cell small-24 medium-13 overflow">
        <div class="cell-margin">

            <h1 class="title">Login</h1>

            <!--HTMl5-->
            <div class="component-buttons">
                <button type="button" class="flip flip-login-html show-code-html"><i class="fab fa-html5"></i> Html</button>
                <button type="button" onClick="CopyToClipboard('to-copy')" id="copied">
                    <i class="far fa-copy"> Copy</i></button>
            </div>


            <!--Panel HTML5-->
            <div class="panel panel-login-html" id="to-copy">
                <pre class="line-numbers language-markup">
                    <code id="to-copy">&lt;div class=&quot;molecule-register&quot;&gt;
                        &lt;div class=&quot;input-group&quot;&gt;
                               &lt;span class=&quot;input-group-label&quot;&gt;&lt;i class=&quot;fas fa-at&quot;&gt;&lt;/i&gt;&lt;/span&gt;
                                &lt;input class=&quot;input-group-field&quot; type=&quot;email&quot; title=&quot;&quot; placeholder=&quot;Email&quot;&gt;
                       &lt;/div&gt;
                    &lt;/div&gt;</code>
                </pre>
            </div>


            <!--SCSS-->
            <div class="component-buttons">
                <button type="button" class="flip flip-login-scss show-code-scss"><i class="fab fa-html5"></i> SCSS</button>
                <button type="button" onClick="CopyToClipboard('to-copy')" id="copied">
                    <i class="far fa-copy"> Copy</i></button>
            </div>

            <!--Panel SCSS-->
            <div class="panel panel-login-scss" id="to-copy">
                <pre class="line-numbers language-scss">
                    <code id="to-copy">.molecule-register {
                      border-radius: 2px;
                      padding: 5px;
                      font-family: 'Bai Jamjuree', sans-serif;
                      width: 300px;
                      label {
                        color: #696768;
                        padding: 5px;
                      }
                      input {
                        border-radius: 2px;
                        border: none;
                      }
                      input[type='button'] {
                        width: 288px;
                        border-radius: 2px;
                        font-family: 'Asap Condensed', sans-serif;
                        letter-spacing: 0.5px;
                      }
                      h1 {
                        text-align: center;
                      }
                      :focus {
                        border: none;
                      }
                      .input-group-label {
                        border: solid 1px #ecf4ff;
                        background: #ffffff;
                      }
                    }</code>
                </pre>
            </div>


        </div>
    </div>
<script>
    Prism.highlightAll();
    CallCopyToClipboard();

    $(document).ready(function () {
        $(".flip-login-html").click(function () {
            $(".panel-login-html").slideToggle("slow");
        });
    });

    $(document).ready(function () {
        $(".flip-login-scss").click(function () {
            $(".panel-login-scss").slideToggle("slow");
        });
    });
</script>


