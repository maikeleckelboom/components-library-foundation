<div class="grid-x master animated fadeIn">
    <div class="cell small-12">
        <div class="cell-margin">

            <!--Horizontal Slider-->
            <div class="slider" data-slider data-initial-start="50" data-end="200">
                <span class="slider-handle"  data-slider-handle role="slider" tabindex="1"></span>
                <span class="slider-fill" data-slider-fill></span>
                <input type="hidden">
            </div>

            <!--Vertical Slider-->
            <div class="slider vertical" data-slider data-initial-start="25" data-end="200" data-vertical="true">
                <span class="slider-handle" data-slider-handle role="slider" tabindex="1"></span>
                <span class="slider-fill" data-slider-fill></span>
                <input type="hidden">
            </div>

            <!--Disabled Slider-->
            <div class="slider disabled" data-slider data-initial-start="78">
                <span class="slider-handle" data-slider-handle role="slider" tabindex="1"></span>
                <span class="slider-fill" data-slider-fill></span>
                <input type="hidden">
            </div>

            <!--Two Handle Sliders-->
            <div class="slider" data-slider data-initial-start="25" data-initial-end="75">
                <span class="slider-handle" data-slider-handle role="slider" tabindex="1"></span>
                <span class="slider-fill" data-slider-fill></span>
                <span class="slider-handle" data-slider-handle role="slider" tabindex="1"></span>
                <input type="hidden">
                <input type="hidden">
            </div>

            <!--Data Binding Slider-->

            <div class="grid-x grid-margin-x">
                <div class="cell small-18">
                    <div class="slider" data-slider data-initial-start="50" ><!--Optional: data-step="5"-->
                        <span class="slider-handle"  data-slider-handle role="slider" tabindex="1" aria-controls="sliderOutput1"></span>
                        <span class="slider-fill" data-slider-fill></span>
                    </div>
                </div>
                <div class="cell small-4">
                    <input title="" type="number" id="sliderOutput1">
                </div>
            </div>
        </div>
    </div>

        <div class="cell small-12">
            <div class="cell-margin">

                <div class="button-group">
                    <button class="flip button show-code flip-slider">Show Code</button>
                    <button onClick="CopyToClipboard('to-copy')" class="button" id="copied">
                        Copy to clipboard
                    </button>
                </div>

                <div class="panel panel-slider" id="to-copy">
        <pre class="line-numbers language-markup">
            <code id="to-copy">&lt;!--Horizontal Slider--&gt;
            &lt;div class=&quot;slider&quot; data-slider data-initial-start=&quot;50&quot; data-end=&quot;200&quot;&gt;
                &lt;span class=&quot;slider-handle&quot; data-slider-handle role=&quot;slider&quot; tabindex=&quot;1&quot;&gt;&lt;/span&gt;
                &lt;span class=&quot;slider-fill&quot; data-slider-fill&gt;&lt;/span&gt;
                &lt;input type=&quot;hidden&quot;&gt;
            &lt;/div&gt;

            &lt;!--Vertical Slider--&gt;
            &lt;div class=&quot;slider vertical&quot; data-slider data-initial-start=&quot;25&quot; data-end=&quot;200&quot; data-vertical=&quot;true&quot;&gt;
                &lt;span class=&quot;slider-handle&quot; data-slider-handle role=&quot;slider&quot; tabindex=&quot;1&quot;&gt;&lt;/span&gt;
                &lt;span class=&quot;slider-fill&quot; data-slider-fill&gt;&lt;/span&gt;
                &lt;input type=&quot;hidden&quot;&gt;
            &lt;/div&gt;

            &lt;!--Disabled Slider--&gt;
            &lt;div class=&quot;slider disabled&quot; data-slider data-initial-start=&quot;78&quot;&gt;
                &lt;span class=&quot;slider-handle&quot; data-slider-handle role=&quot;slider&quot; tabindex=&quot;1&quot;&gt;&lt;/span&gt;
                &lt;span class=&quot;slider-fill&quot; data-slider-fill&gt;&lt;/span&gt;
                &lt;input type=&quot;hidden&quot;&gt;
            &lt;/div&gt;

            &lt;!--Two Handle Sliders--&gt;
            &lt;div class=&quot;slider&quot; data-slider data-initial-start=&quot;25&quot; data-initial-end=&quot;75&quot;&gt;
                &lt;span class=&quot;slider-handle&quot; data-slider-handle role=&quot;slider&quot; tabindex=&quot;1&quot;&gt;&lt;/span&gt;
                &lt;span class=&quot;slider-fill&quot; data-slider-fill&gt;&lt;/span&gt;
                &lt;span class=&quot;slider-handle&quot; data-slider-handle role=&quot;slider&quot; tabindex=&quot;1&quot;&gt;&lt;/span&gt;
                &lt;input type=&quot;hidden&quot;&gt;
                &lt;input type=&quot;hidden&quot;&gt;
            &lt;/div&gt;

            &lt;!--Data Binding Slider--&gt;
            &lt;div class=&quot;grid-x grid-margin-x&quot;&gt;
                &lt;div class=&quot;cell small-18&quot;&gt;
                    &lt;div class=&quot;slider&quot; data-slider data-initial-start=&quot;50&quot;&gt;&lt;!--Optional: data-step=&quot;5&quot;--&gt;
                        &lt;span class=&quot;slider-handle&quot; data-slider-handle role=&quot;slider&quot; tabindex=&quot;1&quot;
                              aria-controls=&quot;sliderOutput1&quot;&gt;&lt;/span&gt;
                        &lt;span class=&quot;slider-fill&quot; data-slider-fill&gt;&lt;/span&gt;
                    &lt;/div&gt;
                &lt;/div&gt;
                &lt;div class=&quot;cell small-4&quot;&gt;
                    &lt;input title=&quot;&quot; type=&quot;number&quot; id=&quot;sliderOutput1&quot;&gt;
                &lt;/div&gt;
            &lt;/div&gt;</code>
        </pre>
                </div>
            </div>
        </div>
</div>


<script>
    Prism.highlightAll();
    CallCopyToClipboard();

    $(document).ready(function () {
        $(".flip-slider").click(function () {
            $(".panel-slider").slideToggle("slow");
        });
    });
</script>