<div class="grid-x master animated fadeIn">
    <div class="cell small-24 medium-11">
        <div class="cell-margin">

            <nav aria-label="You are here:" role="navigation">
                <ul class="breadcrumbs">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Features</a></li>
                    <li class="disabled">Gene Splicing</li>
                    <li>
                        <span class="show-for-sr">Current: </span> Cloning
                    </li>
                </ul>
            </nav>

        </div>
    </div>

    <div class="cell small-24 medium-13">
        <div class="cell-margin">

            <div class="button-group">
                <button class="flip bread-flip button show-code-html">Show Code</button>
                <button onClick="CopyToClipboard('to-copy')" class="button" id="copied">
                    Copy to clipboard
                </button>
            </div>

            <div class="panel bread-panel" id="to-copy">
        <pre class="line-numbers language-markup">
            <code id="save-copy">&lt;nav aria-label=&quot;You are here:&quot; role=&quot;navigation&quot;&gt;
                    &lt;ul class=&quot;breadcrumbs&quot;&gt;
                        &lt;li&gt;&lt;a href=&quot;#&quot;&gt;Home&lt;/a&gt;&lt;/li&gt;
                        &lt;li&gt;&lt;a href=&quot;#&quot;&gt;Features&lt;/a&gt;&lt;/li&gt;
                        &lt;li class=&quot;disabled&quot;&gt;Gene Splicing&lt;/li&gt;
                        &lt;li&gt;
                            &lt;span class=&quot;show-for-sr&quot;&gt;Current: &lt;/span&gt; Cloning
                        &lt;/li&gt;
                    &lt;/ul&gt;
                &lt;/nav&gt;</code>
        </pre>
            </div>

        </div>
    </div>
</div>



<script>
    Prism.highlightAll();
    CallCopyToClipboard();

    $(document).ready(function () {
        $(".bread-flip").click(function () {
            $(".bread-panel").slideToggle("slow");
        });
    });
</script>