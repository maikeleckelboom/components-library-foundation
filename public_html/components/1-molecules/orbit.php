<div class="grid-x master animated fadeIn">
    <div class="cell small-24 medium-11 overflow">
        <div class="cell-margin">
            <div class="orbit" role="region" aria-label="Favorite Space Pictures" data-orbit>
                <div class="orbit-wrapper">

                    <div class="orbit-controls">
                        <button class="orbit-previous"><span class="show-for-sr">Previous Slide</span>&#9664;&#xFE0E;</button>
                        <button class="orbit-next"><span class="show-for-sr">Next Slide</span>&#9654;&#xFE0E;</button>
                    </div>

                    <ul class="orbit-container">
                        <li class="is-active orbit-slide">
                            <figure class="orbit-figure">
                                <img class="orbit-image" src="https://placehold.it/1200x600/999?text=Slide-1" alt="Space">
                                <figcaption class="orbit-caption">Space, the final frontier.</figcaption>
                            </figure>
                        </li>

                        <li class="orbit-slide">
                            <figure class="orbit-figure">
                                <img class="orbit-image" src="https://placehold.it/1200x600/888?text=Slide-2" alt="Space">
                                <figcaption class="orbit-caption">Lets Rocket!</figcaption>
                            </figure>
                        </li>

                        <li class="orbit-slide">
                            <figure class="orbit-figure">
                                <img class="orbit-image" src="https://placehold.it/1200x600/777?text=Slide-3" alt="Space">
                                <figcaption class="orbit-caption">Encapsulating</figcaption>
                            </figure>
                        </li>

                        <li class="orbit-slide">
                            <figure class="orbit-figure">
                                <img class="orbit-image" src="https://placehold.it/1200x600/666&text=Slide-4" alt="Space">
                                <figcaption class="orbit-caption">Outta This World</figcaption>
                            </figure>
                        </li>
                    </ul>

                </div>

                <nav class="orbit-bullets">
                    <button class="is-active" data-slide="0"><span class="show-for-sr">First slide details.</span>
                        <span class="show-for-sr">Current Slide</span>
                    </button>
                    <button data-slide="1"><span class="show-for-sr">Second slide details.</span></button>
                    <button data-slide="2"><span class="show-for-sr">Third slide details.</span></button>
                    <button data-slide="3"><span class="show-for-sr">Fourth slide details.</span></button>
                </nav>
            </div>

        </div>
    </div>
    <div class="cell small-24 medium-13 overflow">
        <div class="cell-margin">

            <!--HTMl5-->
            <div class="component-buttons">
                <button type="button" class="flip flip-orbit-html show-code-html"><i class="fab fa-html5"></i> Html</button>
                <button type="button" onClick="CopyToClipboard('to-copy')" id="copied">
                    <i class="far fa-copy"> Copy</i></button>
            </div>

            <div class="panel panel-orbit-html" id="to-copy">
        <pre class="line-numbers language-markup">
            <code id="to-copy">&lt;div class=&quot;orbit&quot; role=&quot;region&quot; aria-label=&quot;Favorite Space Pictures&quot; data-orbit&gt;
                &lt;div class=&quot;orbit-wrapper&quot;&gt;

                    &lt;div class=&quot;orbit-controls&quot;&gt;
                        &lt;button class=&quot;orbit-previous&quot;&gt;&lt;span class=&quot;show-for-sr&quot;&gt;Previous Slide&lt;/span&gt;&amp;#9664;&amp;#xFE0E;&lt;/button&gt;
                        &lt;button class=&quot;orbit-next&quot;&gt;&lt;span class=&quot;show-for-sr&quot;&gt;Next Slide&lt;/span&gt;&amp;#9654;&amp;#xFE0E;&lt;/button&gt;
                    &lt;/div&gt;

                    &lt;ul class=&quot;orbit-container&quot;&gt;
                        &lt;li class=&quot;is-active orbit-slide&quot;&gt;
                            &lt;figure class=&quot;orbit-figure&quot;&gt;
                                &lt;img class=&quot;orbit-image&quot; src=&quot;https://placehold.it/1200x600/999?text=Slide-1&quot; alt=&quot;Space&quot;&gt;
                                &lt;figcaption class=&quot;orbit-caption&quot;&gt;Space, the final frontier.&lt;/figcaption&gt;
                            &lt;/figure&gt;
                        &lt;/li&gt;

                        &lt;li class=&quot;orbit-slide&quot;&gt;
                            &lt;figure class=&quot;orbit-figure&quot;&gt;
                                &lt;img class=&quot;orbit-image&quot; src=&quot;https://placehold.it/1200x600/888?text=Slide-2&quot; alt=&quot;Space&quot;&gt;
                                &lt;figcaption class=&quot;orbit-caption&quot;&gt;Lets Rocket!&lt;/figcaption&gt;
                            &lt;/figure&gt;
                        &lt;/li&gt;

                        &lt;li class=&quot;orbit-slide&quot;&gt;
                            &lt;figure class=&quot;orbit-figure&quot;&gt;
                                &lt;img class=&quot;orbit-image&quot; src=&quot;https://placehold.it/1200x600/777?text=Slide-3&quot; alt=&quot;Space&quot;&gt;
                                &lt;figcaption class=&quot;orbit-caption&quot;&gt;Encapsulating&lt;/figcaption&gt;
                            &lt;/figure&gt;
                        &lt;/li&gt;

                        &lt;li class=&quot;orbit-slide&quot;&gt;
                            &lt;figure class=&quot;orbit-figure&quot;&gt;
                                &lt;img class=&quot;orbit-image&quot; src=&quot;https://placehold.it/1200x600/666&amp;text=Slide-4&quot; alt=&quot;Space&quot;&gt;
                                &lt;figcaption class=&quot;orbit-caption&quot;&gt;Outta This World&lt;/figcaption&gt;
                            &lt;/figure&gt;
                        &lt;/li&gt;
                    &lt;/ul&gt;

                &lt;/div&gt;

                &lt;nav class=&quot;orbit-bullets&quot;&gt;
                    &lt;button class=&quot;is-active&quot; data-slide=&quot;0&quot;&gt;&lt;span class=&quot;show-for-sr&quot;&gt;First slide details.&lt;/span&gt;
                        &lt;span class=&quot;show-for-sr&quot;&gt;Current Slide&lt;/span&gt;
                    &lt;/button&gt;
                    &lt;button data-slide=&quot;1&quot;&gt;&lt;span class=&quot;show-for-sr&quot;&gt;Second slide details.&lt;/span&gt;&lt;/button&gt;
                    &lt;button data-slide=&quot;2&quot;&gt;&lt;span class=&quot;show-for-sr&quot;&gt;Third slide details.&lt;/span&gt;&lt;/button&gt;
                    &lt;button data-slide=&quot;3&quot;&gt;&lt;span class=&quot;show-for-sr&quot;&gt;Fourth slide details.&lt;/span&gt;&lt;/button&gt;
                &lt;/nav&gt;
            &lt;/div&gt;</code>
        </pre>
            </div>

        </div>
    </div>
</div>






<script>
    Prism.highlightAll();
    CallCopyToClipboard();

    $(document).ready(function () {
        $(".flip-orbit-html").click(function () {
            $(".panel-orbit-html").slideToggle("slow");
        });
    });
</script>