<div class="grid-x master animated fadeIn">
    <div class="cell small-24 medium-11 overflow">
        <div class="cell-margin">
            <div class="media-object stack-for-small">
                <div class="media-object-section">
                    <div class="thumbnail">
                        <img src= "images/600x300.png">
                    </div>
                </div>

                <div class="media-object-section">
                    <h4>I Can Stack.</h4>
                    <p>Shrink the browser width to see me stack. I do tricks for dog treats, but I'm not a dog.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="cell small-24 medium-13 overflow">
        <div class="cell-margin">

<!--            <div class="button-group">-->
<!--                <button class="flip button show-code flip-media">Show Code</button>-->
<!--                <button onClick="CopyToClipboard('to-copy')" class="button" id="copied">-->
<!--                    Copy to clipboard-->
<!--                </button>-->
<!--            </div>-->

            <div class="component-buttons">
                <button type="button" class="flip flip-media show-code-html"><i class="fas fa-code"></i> Html</button>
                <button type="button" onClick="CopyToClipboard('to-copy')" id="copied">
                    <i class="far fa-copy"></i> Copy</button>
            </div>


            <div class="panel panel-media" id="to-copy" >
                <pre class="line-numbers language-markup">
                    <code id="to-copy">&lt;div class=&quot;media-object stack-for-small&quot;&gt;
                        &lt;div class=&quot;media-object-section&quot;&gt;
                            &lt;div class=&quot;thumbnail&quot;&gt;
                                &lt;img src= &quot;images/600x300.png&quot;&gt;
                            &lt;/div&gt;
                        &lt;/div&gt;

                        &lt;div class=&quot;media-object-section&quot;&gt;
                            &lt;h4&gt;I Can Stack.&lt;/h4&gt;
                            &lt;p&gt;Shrink the browser width to see me stack.&lt;/p&gt;
                        &lt;/div&gt;
                    &lt;/div&gt;</code>
                </pre>
            </div>
        </div>
    </div>
</div>








<script>
    Prism.highlightAll();
    CallCopyToClipboard();

    $(document).ready(function () {
        $(".flip-media").click(function () {
            $(".panel-media").slideToggle("slow");
        });
    });
</script>