<div class="grid-x animated fadeIn">
    <div class="cell small-12">
        <div class="cell-margin">

            <ul class="tabs" data-active-collapse="true" data-tabs id="collapsing-tabs">
                <li class="tabs-title is-active"><a href="#panel1c" aria-selected="true">Tab 1</a></li>
                <li class="tabs-title"><a href="#panel2c">Tab 2</a></li>
                <li class="tabs-title"><a href="#panel3c">Tab 3</a></li>
                <li class="tabs-title"><a href="#panel4c">Tab 4</a></li>
            </ul>

            <div class="tabs-content" data-tabs-content="collapsing-tabs">
                <div class="tabs-panel is-active" id="panel1c">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua.</p>
                </div>

                <div class="tabs-panel" id="panel2c">
                    <p>Vivamus hendrerit arcu sed erat molestie vehicula. Sed auctor neque eu tellus rhoncus ut eleifend
                        nibh porttitor. Ut in nulla enim. Phasellus molestie magna non est bibendum non venenatis nisl
                        tempor. Suspendisse dictum feugiat nisl ut dapibus.</p>
                </div>

                <div class="tabs-panel" id="panel3c">
                    <img class="thumbnail" src="">
                </div>

                <div class="tabs-panel" id="panel4c">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua.</p>
                </div>
            </div>

        </div>
    </div>
    <div class="cell small-12">
        <div class="cell-margin">

            <div class="button-group">
                <button class="flip button show-code flip-tabs">Show Code</button>
                <button onClick="CopyToClipboard('to-copy')" class="button" id="copied">
                    Copy to clipboard
                </button>
            </div>

            <div class="panel panel-tabs" id="to-copy">
        <pre class="line-numbers language-markup">
            <code id="to-copy">&lt;ul class=&quot;tabs&quot; data-active-collapse=&quot;true&quot; data-tabs id=&quot;collapsing-tabs&quot;&gt;
            &lt;li class=&quot;tabs-title is-active&quot;&gt;&lt;a href=&quot;#panel1c&quot; aria-selected=&quot;true&quot;&gt;Tab 1&lt;/a&gt;&lt;/li&gt;
            &lt;li class=&quot;tabs-title&quot;&gt;&lt;a href=&quot;#panel2c&quot;&gt;Tab 2&lt;/a&gt;&lt;/li&gt;
            &lt;li class=&quot;tabs-title&quot;&gt;&lt;a href=&quot;#panel3c&quot;&gt;Tab 3&lt;/a&gt;&lt;/li&gt;
            &lt;li class=&quot;tabs-title&quot;&gt;&lt;a href=&quot;#panel4c&quot;&gt;Tab 4&lt;/a&gt;&lt;/li&gt;
            &lt;/ul&gt;

            &lt;div class=&quot;tabs-content&quot; data-tabs-content=&quot;collapsing-tabs&quot;&gt;
            &lt;div class=&quot;tabs-panel is-active&quot; id=&quot;panel1c&quot;&gt;
            &lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.&lt;/p&gt;
            &lt;/div&gt;

            &lt;div class=&quot;tabs-panel&quot; id=&quot;panel2c&quot;&gt;
            &lt;p&gt;Vivamus hendrerit arcu sed erat molestie vehicula. Sed auctor neque eu tellus rhoncus ut eleifend nibh porttitor. Ut in nulla enim. Phasellus molestie magna non est bibendum non venenatis nisl tempor. Suspendisse dictum feugiat nisl ut dapibus.&lt;/p&gt;
            &lt;/div&gt;

            &lt;div class=&quot;tabs-panel&quot; id=&quot;panel3c&quot;&gt;
            &lt;img class=&quot;thumbnail&quot; src=&quot;&quot;&gt;
            &lt;/div&gt;

            &lt;div class=&quot;tabs-panel&quot; id=&quot;panel4c&quot;&gt;
            &lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.&lt;/p&gt;
            &lt;/div&gt;
            &lt;/div&gt;</code>
        </pre>
            </div>
        </div>
    </div>
</div>

<script>
    Prism.highlightAll();
    CallCopyToClipboard();

    $(document).ready(function () {
        $(".flip-tabs").click(function () {
            $(".panel-tabs").slideToggle("slow");
        });
    });
</script>