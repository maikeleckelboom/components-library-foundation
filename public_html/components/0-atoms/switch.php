<div class="grid-x master animated fadeIn">
    <div class="cell small-12">
        <div class="cell-margin">

        </div>
    </div>
    <div class="cell small-12">
        <div class="cell-margin">

        </div>
    </div>
</div>
<!--    <p class="margin-left">Switch:<a href="https://foundation.zurb.com/sites/docs/switch.html" target="_blank">https://foundation.zurb.com/sites/docs/switch.html</a>-->
<!--    </p>-->


    <!--Default switch-->
    <div class="switch">
        <input class="switch-input" id="exampleSwitch" type="checkbox" name="exampleSwitch">
        <label class="switch-paddle" for="exampleSwitch">
            <span class="show-for-sr">Download Kittens</span>
        </label>
    </div>

    <!--Radio Switch-->
    <div class="switch">
        <input class="switch-input" id="exampleRadioSwitch1" type="radio" checked name="testGroup">
        <label class="switch-paddle" for="exampleRadioSwitch1">
            <span class="show-for-sr">Bulbasaur</span>
        </label>
    </div>

    <!--Switch with sizing classes-->
    <div class="switch tiny">
        <input class="switch-input" id="tinySwitch" type="checkbox" name="exampleSwitch">
        <label class="switch-paddle" for="tinySwitch">
            <span class="show-for-sr">Tiny Sandwiches Enabled</span>
        </label>
    </div>

    <div class="switch small">
        <input class="switch-input" id="smallSwitch" type="checkbox" name="exampleSwitch">
        <label class="switch-paddle" for="smallSwitch">
            <span class="show-for-sr">Small Portions Only</span>
        </label>
    </div>

    <div class="switch large">
        <input class="switch-input" id="largeSwitch" type="checkbox" name="exampleSwitch">
        <label class="switch-paddle" for="largeSwitch">
            <span class="show-for-sr">Show Large Elephants</span>
        </label>
    </div>

    <!--Inner labels-->
    <p>Do you like me?</p>
    <div class="switch large">
        <input class="switch-input" id="yes-no" type="checkbox" name="exampleSwitch">
        <label class="switch-paddle" for="yes-no">
            <span class="switch-active" aria-hidden="true">Yes</span>
            <span class="switch-inactive" aria-hidden="true">No</span>
        </label>
    </div>

</section>
