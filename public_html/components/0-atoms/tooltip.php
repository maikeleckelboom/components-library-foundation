<div class="grid-x master animated fadeIn">
    <div class="cell small-12">
        <div class="cell-margin">
            <section class="window">
                <button class="button" type="button" data-tooltip tabindex="1" title="Fancy word for a beetle." data-position="bottom" data-alignment="left">
                    Bottom Left
                </button>

                <button class="button" type="button"  data-tooltip tabindex="1" title="Fancy word for a beetle." data-position="bottom" data-alignment="center">
                    Bottom Center
                </button>

                <button class="button" type="button" data-tooltip tabindex="1" title="Fancy word for a beetle." data-position="bottom" data-alignment="right">
                    Bottom Right
                </button>

                <button class="button" type="button" data-tooltip tabindex="1" title="Fancy word for a beetle." data-position="top" data-alignment="left">
                    Top Left
                </button>

                <button class="button" type="button" data-tooltip tabindex="1" title="Fancy word for a beetle." data-position="top" data-alignment="center">
                    Top Center
                </button>

                <button class="button" type="button" data-tooltip tabindex="1" title="Fancy word for a beetle." data-position="top" data-alignment="right">
                    Top Right
                </button>
            </section>
        </div>
    </div>
    <div class="cell small-12">
        <div class="cell-margin">

        </div>
    </div>
</div>


