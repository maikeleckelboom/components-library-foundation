<div class="grid-x master animated fadeIn">
    <div class="cell small-12">
        <div class="cell-margin">
            <span class="badge" style="font-size: 1rem">1</span>
            <span class="badge secondary" style="font-size: 1rem">2</span>
            <span class="badge success" style="font-size: 1rem">3</span>
            <span class="badge warning" style="font-size: 1rem">4</span>
            <span class="badge alert" style="font-size: 1rem">5</span>

            <span class="label" style="font-size: 1rem">1</span>
            <span class="label secondary" style="font-size: 1rem">2</span>
            <span class="label success" style="font-size: 1rem">3</span>
            <span class="label warning" style="font-size: 1rem">4</span>
            <span class="label alert" style="font-size: 1rem">5</span>

        </div>
    </div>
    <div class="cell small-12">
        <div class="cell-margin">
            <p class="flip button badge-flip">Show Code</p>
            <div class="panel badge-panel">
            <pre>&lt;span class=&quot;badge&quot; style=&quot;font-size: 1rem&quot;&gt;1&lt;/span&gt;
&lt;span class=&quot;badge secondary&quot; style=&quot;font-size: 1rem&quot;&gt;2&lt;/span&gt;
&lt;span class=&quot;badge success&quot; style=&quot;font-size: 1rem&quot;&gt;3&lt;/span&gt;
&lt;span class=&quot;badge warning&quot; style=&quot;font-size: 1rem&quot;&gt;4&lt;/span&gt;
&lt;span class=&quot;badge alert&quot; style=&quot;font-size: 1rem&quot;&gt;5&lt;/span&gt;

&lt;span class=&quot;label&quot; style=&quot;font-size: 1rem&quot;&gt;1&lt;/span&gt;
&lt;span class=&quot;label secondary&quot; style=&quot;font-size: 1rem&quot;&gt;2&lt;/span&gt;
&lt;span class=&quot;label success&quot; style=&quot;font-size: 1rem&quot;&gt;3&lt;/span&gt;
&lt;span class=&quot;label warning&quot; style=&quot;font-size: 1rem&quot;&gt;4&lt;/span&gt;
&lt;span class=&quot;label alert&quot; style=&quot;font-size: 1rem&quot;&gt;5&lt;/span&gt;=&quot;font-size: 1rem&quot;&gt;5&lt;/span&gt;
            </pre>
            </div>
        </div>
    </div>
</div>




<script>
    $(document).ready(function () {
        $(".badge-flip").click(function () {
            $(".badge-panel").slideToggle("slow");
        });
    });
</script>