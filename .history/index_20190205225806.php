<!--    Includes    -->
<?php include 'components/-base/head.html' ?>
<?php include 'components/-base/off-canvas.html'; ?>
<?php include 'components/-base/settings.html' ?>



<div class="off-canvas-content" data-off-canvas-content >
    <?php include 'components/-base/header.html'; ?>


    <div class="grid-x">
        
        <div class="cell small-24">

            <div id="welcome-container">
                <?php include 'components/-base/welcome.html' ?>
            </div>

            <div class="parallax-vendor">
                <?php include 'components/vendors/parallax.php' ?>
            </div>

            <!-- Foundation-->
            <div id="badge"></div>
            <div id="buttons-container"></div>
            <div id="form-container"></div>
            <div id="switch-container"></div>
            <div id="tooltip-container"></div>
            <div id="badges-container"></div>
            <div id="accordion-container"></div>
            <div id="breadcrumbs-container"></div>
            <div id="card-container"></div>
            <div id="callout-container"></div>
            <div id="close-button-container"></div>
            <div id="dropdown-container"></div>
            <div id="login-container"></div>
            <div id="magellan-container"></div>
            <div id="media-object-container"></div>
            <div id="orbit-container"></div>
            <div id="off-canvas-container"></div>
            <div id="pagination-container"></div>
            <div id="progress-container"></div>
            <div id="register-container"></div>
            <div id="reveal-container"></div>
            <div id="responsive-accordion"></div>
            <div id="slider-container"></div>
            <div id="table-container"></div>
            <div id="tabs-container"></div>

            <!-- Atoms -->
            <div id="back_to_top-container"></div>
            <div id="thumbnail-container"></div>

        </div>


        <script>

            (function CopyThis(){

                var pre = document.getElementsByTagName('pre');

                for (var i = 0; i < pre.length; i++) {
                    var isLanguage = pre[i].children[0].className.indexOf('language-');
                    if ( isLanguage === 0 ) {
                        var button           = document.createElement('button');
                        button.className = 'copy-button';
                        button.textContent = 'Copy';

                        pre[i].appendChild(button);
                    }

                    var copyCode = new Clipboard('.copy-button', {
                        target: function(trigger) {
                            return trigger.previousElementSibling;
                        }
                    });
                }

                copyCode.on('success', function(event) {
                    event.clearSelection();
                    event.trigger.textContent = 'Copied';
                    window.setTimeout(function() {
                        event.trigger.textContent = "Copy";
                    }, 2000);
                });

                copyCode.on('error', function(event) {
                    event.trigger.textContent = 'Press "Ctrl + C" to copy';
                    window.setTimeout(function() {
                        event.trigger.textContent = 'Copy';
                    }, 2000);
                });

            })();

        </script>

        <?php include 'components/-base/footer.html' ?>
