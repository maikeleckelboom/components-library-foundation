<!--    Includes    -->
<?php include 'components/-base/head.html' ?>
<?php include 'components/-base/off-canvas.html'; ?>
<?php include 'components/-base/settings.html' ?>



<div class="off-canvas-content" data-off-canvas-content >
    <?php include 'components/-base/header.html'; ?>


    <div class="grid-x">
        
        <div class="cell small-24">

        <ul id="myTabs"s class="tabs" data-options="deep_linking:true" data-tab>
  <li class="tab-title active"><a href="#panel1">Tab 1</a></li>
  <li data-url="test2" class="tab-title"><a href="#panel2">Tab 2</a></li>
  <li data-url="test3" class="tab-title"><a href="#panel3">Tab 3</a></li>
  <li class="tab-title"><a href="#panel4">Tab 4</a></li>
</ul>
<div class="tabs-content">
  <div class="content active" id="panel1">
    <p>This is the first panel of the basic tab example. You can place all sorts of content here including a grid.</p>
  </div>
  <div class="content" id="panel2">
    <p>This is the second panel of the basic tab example. This is the second panel of the basic tab example.</p>
  </div>
  <div class="content" id="panel3">
    <p>This is the third panel of the basic tab example. This is the third panel of the basic tab example.</p>
  </div>
  <div class="content" id="panel4">
    <p>This is the fourth panel of the basic tab example. This is the fourth panel of the basic tab example.</p>
  </div>
</div>

            <div id="welcome-container">
                <?php include 'components/-base/welcome.html' ?>
            </div>

            <div class="parallax-vendor">
                <?php include 'components/vendors/parallax.php' ?>
            </div>

            <!-- Foundation-->
            <div id="badge"></div>
            <div id="buttons-container"></div>
            <div id="form-container"></div>
            <div id="switch-container"></div>
            <div id="tooltip-container"></div>
            <div id="badges-container"></div>
            <div id="accordion-container"></div>
            <div id="breadcrumbs-container"></div>
            <div id="card-container"></div>
            <div id="callout-container"></div>
            <div id="close-button-container"></div>
            <div id="dropdown-container"></div>
            <div id="login-container"></div>
            <div id="magellan-container"></div>
            <div id="media-object-container"></div>
            <div id="orbit-container"></div>
            <div id="off-canvas-container"></div>
            <div id="pagination-container"></div>
            <div id="progress-container"></div>
            <div id="register-container"></div>
            <div id="reveal-container"></div>
            <div id="responsive-accordion"></div>
            <div id="slider-container"></div>
            <div id="table-container"></div>
            <div id="tabs-container"></div>

            <!-- Atoms -->
            <div id="back_to_top-container"></div>
            <div id="thumbnail-container"></div>

        </div>


        <script>

            (function CopyThis(){

                var pre = document.getElementsByTagName('pre');

                for (var i = 0; i < pre.length; i++) {
                    var isLanguage = pre[i].children[0].className.indexOf('language-');
                    if ( isLanguage === 0 ) {
                        var button           = document.createElement('button');
                        button.className = 'copy-button';
                        button.textContent = 'Copy';

                        pre[i].appendChild(button);
                    }

                    var copyCode = new Clipboard('.copy-button', {
                        target: function(trigger) {
                            return trigger.previousElementSibling;
                        }
                    });
                }

                copyCode.on('success', function(event) {
                    event.clearSelection();
                    event.trigger.textContent = 'Copied';
                    window.setTimeout(function() {
                        event.trigger.textContent = "Copy";
                    }, 2000);
                });

                copyCode.on('error', function(event) {
                    event.trigger.textContent = 'Press "Ctrl + C" to copy';
                    window.setTimeout(function() {
                        event.trigger.textContent = 'Copy';
                    }, 2000);
                });

            })();

        </script>

        <script>
            </script>

        <?php include 'components/-base/footer.html' ?>
