<!--    Includes    -->
<?php include 'components/includes/head.html' ?>
<?php include 'components/includes/off-canvas.html'; ?>
<?php include 'components/includes/settings_modal.html' ?>
<script src="components/vendors/parallax.js-1.5.0/parallax.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<!--<link rel="stylesheet" href="components/vendors/OwlCarousel2-2.3.4/docs/assets/css/docs.theme.min.css">-->

<div class="off-canvas-content" data-off-canvas-content>
    <?php include 'components/includes/header.html'; ?>


    <div class="grid-x">

        <div class="cell small-24">
        <div id="welcome-container">
            <?php include 'components/includes/welcome.html' ?>

            <div class="vendors">

                <div class="parallax-vendor">
<!--                    --><?php //include 'components/vendors/parallax.php' ?>
                </div>

                <div class="carousel-vendor">
                    <div id="parallax-container"></div>

                </div>
            </div>



        </div>

        <div id="section-ajax">
            <!-- Atoms -->
            <div id="badge"></div>
            <div id="buttons-container"></div>
            <div id="form-container"></div>
            <div id="switch-container"></div>
            <div id="tooltip-container"></div>
            <div id="badges-container"></div>

            <!-- Molecules -->
            <div id="accordion-container"></div>
            <div id="breadcrumbs-container"></div>
            <div id="card-container"></div>


            <div id="callout-container"></div>
            <div id="close-button-container"></div>
            <div id="dropdown-container"></div>
            <div id="login-container"></div>
            <div id="magellan-container"></div>
            <div id="media-object-container"></div>
            <div id="orbit-container"></div>
            <div id="off-canvas-container"></div>
            <div id="pagination-container"></div>
            <div id="progress-container"></div>
            <div id="register-container"></div>
            <div id="reveal-container"></div>
            <div id="responsive-accordion"></div>
            <div id="slider-container"></div>
            <div id="thumbnail-container"></div>
            <div id="table-container"></div>
            <div id="tabs-container"></div>

            <div id="back_to_top-container"></div>
        </div>
    </div>


    <?php include 'components/includes/footer.html' ?>
