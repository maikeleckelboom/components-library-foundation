<div class="grid-x master animated fadeIn">

    <div class="cell medium-11 small-24">
        <div class="cell-margin">
            <div class="thumbnail">

                <div class="zoom">
                    <img src="images/atomic-land.jpeg" alt="">
                </div>

            </div>
        </div>
    </div>

    <div class="cell medium-13 small-24 overflow">
        <div class="cell-margin">


            <h1 class="title">Thumbnail Zoom</h1>


            <div class="component-buttons">
                <button type="button" class="flip flip-thumb-html show-code-html"><i class="fab fa-html5"></i><span class="lang-html">HTML</span></button>
                <button type="button" onClick="CopyToClipboard('to-copy')" id="copied">
                    <i class="far fa-copy"> Copy</i></button>
            </div>


            <div class="panel panel-thumb-html" id="to-copy">
                <pre class="line-numbers language-markup">
                    <code id="to-copy">&lt;div class=&quot;thumbnail&quot;&gt;
                        &lt;div class=&quot;zoom&quot;&gt;
                            &lt;img src=&quot;images/atomic-land.jpeg&quot; alt=&quot;&quot;&gt;
                        &lt;/div&gt;
                    &lt;/div&gt;</code>
                </pre>
            </div>


            <div class="component-buttons">

                <button type="button" class="flip flip-thumb-scss show-code-scss"><i class="fab fa-sass"></i><span class="lang-scss">SCSS</span></button>
                <button type="button" class="copy-scss" onClick="CopyToClipboard('to-copy')" id="copied">
                    <i class="far fa-copy"> Copy</i></button>
            </div>

                <div class="panel panel-thumb-scss" id="to-copy">
                    <pre class="line-numbers language-scss">
                        <code id="to-copy">.thumbnail {
                          display: block;
                          background-color: #fff;
                          overflow: hidden;
                          border: none;
                        }

                        .thumbnail .zoom {
                          overflow:hidden;
                          margin: 0 auto;
                          z-index: 3;
                          position: relative;
                          border: none;
                        }

                        .thumbnail img {
                          background-color: transparent;
                          -webkit-transition: all .7s ease-in-out;
                          -moz-transition: all .7s ease-in-out;
                          -o-transition: all .7s ease-in-out;
                          transition: all .7s ease-in-out;
                          z-index: 2;
                          border: none;
                        }

                        .thumbnail .zoom:hover img {
                          -webkit-transform:scale(1.1);
                          transform:scale(1.1);
                          height: calc(100% + 200px);
                          width: calc(100% + 200px);
                          border: none;
                        }</code>
                    </pre>
                </div>

            </div>

    </div>

</div>

<script>
    Prism.highlightAll();
    CallCopyToClipboard();

    $(document).ready(function () {
        $(".flip-thumb-html").click(function () {
            $(".panel-thumb-html").slideToggle("slow");
        });

        $(".flip-thumb-scss").click(function () {
            $(".panel-thumb-scss").slideToggle("slow");
        });

        $(".flip-thumb-js").click(function () {
            $(".panel-thumb-js").slideToggle("slow");
        });
    });
</script>