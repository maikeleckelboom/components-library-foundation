<div class="grid-x master animated fadeIn">

    <div id="inner-content">
        <div class="corner topLeft"></div>
        <div class="corner topRight"></div>
        <div class="corner bottomLeft"></div>
        <div class="corner bottomRight"></div>

        <div class="cell small-24">
            <div class="cell-margin">
                <div class="parallax-window" data-parallax="scroll" data-image-src="files/bricks.jpg"
                     data-z-index="1"></div>
            </div>
        </div>

        <div class="cell small-20">
            <div class="cell-margin">

                <h1 class="title">Parallax.js</h1>
                <p class="link"><a href="http://pixelcog.github.io/parallax.js/" target="_blank">http://pixelcog.github.io/parallax.js/</a>
                </p>

                <div class="code-languages">
                    <div class="scripts float-shadow cta">
    <pre class="line-numbers language-markup">
    <code>&lt;script src=&quot;https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js&quot;&gt;&lt;/script&gt;</code>
    <code>&lt;script src=&quot;/path/to/parallax.js&quot;&gt;&lt;/script&gt;</code>
    </pre>
                        <button class="copy-button"><span>Copy</span></button>
                    </div>

                    <div class="html float-shadow cta">
    <pre class="line-numbers language-markup">
    <code>&lt;div class=&quot;parallax-window&quot; data-parallax=&quot;scroll&quot; data-image-src=&quot;/path/to/image.jpg&quot;&gt;&lt;/div&gt;</code>
    </pre>
                        <button class="copy-button">Copy</button>
                    </div>

                    <div class="scss float-shadow cta">
    <pre class="line-numbers language-css">
    <code>.parallax-window {
        min-height: 400px;
        background: transparent;
     }</code>
    </pre>
                        <button class="copy-button">Copy</button>
                    </div>

                </div><!--  X inner-content  -->

            </div>
        </div>

    </div>
</div>


