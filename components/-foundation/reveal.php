
<div class="grid-x master animated fadeIn">
    <div class="cell small-24 medium-11 overflow">
        <div class="cell-margin">
            <!--Default-->
            <div class="reveal" id="exampleModal1" data-reveal>
                <h1>Awesome. I Have It.</h1>
                <p class="lead">Your couch. It is mine.</p>
                <p>I'm a cool paragraph that lives inside of an even cooler modal. Wins!</p>
                <button class="close-button" data-close aria-label="Close modal" type="button">
                    <span aria-hidden="true">&times;</span>
                </button>
                <button class="close-button" data-close aria-label="Close modal" type="button">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <p><button class="button" data-open="exampleModal1">Click me for a modal</button></p>

            <!--Nested modal-->
            <p><button class="button" data-open="exampleModal2">Click me for a nested modal</button></p>

            <!-- This is the first modal -->
            <div class="reveal" id="exampleModal2" data-reveal>
                <h1>Awesome!</h1>
                <p class="lead">I have another modal inside of me!</p>
                <button class="button" data-open="exampleModal3">Click me for another modal!</button>
                <button class="close-button" data-close aria-label="Close reveal" type="button">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <!-- This is the nested modal -->
            <div class="reveal" id="exampleModal3" data-reveal>
                <h2>ANOTHER MODAL!!!</h2>
                <button class="close-button" data-close aria-label="Close reveal" type="button">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <!--Overlay modal-->
            <p><button class="button" data-toggle="exampleModal9">Click me for an overlay-lacking modal</button></p>

            <div class="reveal" id="exampleModal9" data-reveal data-overlay="false">
                <p>I feel so free!</p>
                <button class="close-button" data-close aria-label="Close reveal" type="button">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <!--Fullscreen Modal-->
            <p><button class="button" data-toggle="exampleModal8">Click me for a full-screen modal</button></p>

            <div class="full reveal" id="exampleModal8" data-reveal>
                <p>OH I'M SO FUUUUL</p>
                <img src="https://placekitten.com/1920/1280" alt="Introspective Cage">
                <button class="close-button" data-close aria-label="Close reveal" type="button">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <!--Animated-->
            <p><button class="button" data-toggle="animatedModal10">Click me for a modal</button></p>

            <div class="reveal" id="animatedModal10" data-reveal data-close-on-click="true" data-animation-in="spin-in" data-animation-out="spin-out">
                <h1>Whoa, I'm dizzy!</h1>
                <p class="lead">There are many options for animating modals, check out the Motion UI library to see them all</p>
                <button class="close-button" data-close aria-label="Close reveal" type="button">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

        </div>
    </div>

    <div class="cell small-24 medium-13 overflow">
        <div class="cell-margin">


            <h1 class="title">
                Reveal Modal
            </h1>

            <!--HTMl5-->
            <div class="component-buttons">
                <button type="button" class="flip flip-reveal-html show-code-html"><i class="fab fa-html5"></i> Html</button>
                <button type="button" onClick="CopyToClipboard('to-copy')" id="copied">
                    <i class="far fa-copy"> Copy</i></button>
            </div>

            <div class="panel panel-reveal-html" id="to-copy">
                <pre class="line-numbers language-markup">
                    <code id="to-copy">&lt;!--Default--&gt;
                        &lt;div class=&quot;reveal&quot; id=&quot;exampleModal1&quot; data-reveal&gt;
                            &lt;h1&gt;Awesome. I Have It.&lt;/h1&gt;
                            &lt;p class=&quot;lead&quot;&gt;Your couch. It is mine.&lt;/p&gt;
                            &lt;p&gt;I'm a cool paragraph that lives inside of an even cooler modal. Wins!&lt;/p&gt;
                            &lt;button class=&quot;close-button&quot; data-close aria-label=&quot;Close modal&quot; type=&quot;button&quot;&gt;
                                &lt;span aria-hidden=&quot;true&quot;&gt;&amp;times;&lt;/span&gt;
                            &lt;/button&gt;
                            &lt;button class=&quot;close-button&quot; data-close aria-label=&quot;Close modal&quot; type=&quot;button&quot;&gt;
                                &lt;span aria-hidden=&quot;true&quot;&gt;&amp;times;&lt;/span&gt;
                            &lt;/button&gt;
                        &lt;/div&gt;

                        &lt;p&gt;&lt;button class=&quot;button&quot; data-open=&quot;exampleModal1&quot;&gt;Click me for a modal&lt;/button&gt;&lt;/p&gt;

                        &lt;!--Nested modal--&gt;
                        &lt;p&gt;&lt;button class=&quot;button&quot; data-open=&quot;exampleModal2&quot;&gt;Click me for a nested modal&lt;/button&gt;&lt;/p&gt;

                        &lt;!-- This is the first modal --&gt;
                        &lt;div class=&quot;reveal&quot; id=&quot;exampleModal2&quot; data-reveal&gt;
                            &lt;h1&gt;Awesome!&lt;/h1&gt;
                            &lt;p class=&quot;lead&quot;&gt;I have another modal inside of me!&lt;/p&gt;
                            &lt;button class=&quot;button&quot; data-open=&quot;exampleModal3&quot;&gt;Click me for another modal!&lt;/button&gt;
                            &lt;button class=&quot;close-button&quot; data-close aria-label=&quot;Close reveal&quot; type=&quot;button&quot;&gt;
                                &lt;span aria-hidden=&quot;true&quot;&gt;&amp;times;&lt;/span&gt;
                            &lt;/button&gt;
                        &lt;/div&gt;

                        &lt;!-- This is the nested modal --&gt;
                        &lt;div class=&quot;reveal&quot; id=&quot;exampleModal3&quot; data-reveal&gt;
                            &lt;h2&gt;ANOTHER MODAL!!!&lt;/h2&gt;
                            &lt;button class=&quot;close-button&quot; data-close aria-label=&quot;Close reveal&quot; type=&quot;button&quot;&gt;
                                &lt;span aria-hidden=&quot;true&quot;&gt;&amp;times;&lt;/span&gt;
                            &lt;/button&gt;
                        &lt;/div&gt;

                        &lt;!--Overlay modal--&gt;
                        &lt;p&gt;&lt;button class=&quot;button&quot; data-toggle=&quot;exampleModal9&quot;&gt;Click me for an overlay-lacking modal&lt;/button&gt;&lt;/p&gt;

                        &lt;div class=&quot;reveal&quot; id=&quot;exampleModal9&quot; data-reveal data-overlay=&quot;false&quot;&gt;
                            &lt;p&gt;I feel so free!&lt;/p&gt;
                            &lt;button class=&quot;close-button&quot; data-close aria-label=&quot;Close reveal&quot; type=&quot;button&quot;&gt;
                                &lt;span aria-hidden=&quot;true&quot;&gt;&amp;times;&lt;/span&gt;
                            &lt;/button&gt;
                        &lt;/div&gt;

                        &lt;!--Fullscreen Modal--&gt;
                        &lt;p&gt;&lt;button class=&quot;button&quot; data-toggle=&quot;exampleModal8&quot;&gt;Click me for a full-screen modal&lt;/button&gt;&lt;/p&gt;

                        &lt;div class=&quot;full reveal&quot; id=&quot;exampleModal8&quot; data-reveal&gt;
                            &lt;p&gt;OH I'M SO FUUUUL&lt;/p&gt;
                            &lt;img src=&quot;https://placekitten.com/1920/1280&quot; alt=&quot;Introspective Cage&quot;&gt;
                            &lt;button class=&quot;close-button&quot; data-close aria-label=&quot;Close reveal&quot; type=&quot;button&quot;&gt;
                                &lt;span aria-hidden=&quot;true&quot;&gt;&amp;times;&lt;/span&gt;
                            &lt;/button&gt;
                        &lt;/div&gt;

                        &lt;!--Animated--&gt;
                        &lt;p&gt;&lt;button class=&quot;button&quot; data-toggle=&quot;animatedModal10&quot;&gt;Click me for a modal&lt;/button&gt;&lt;/p&gt;

                        &lt;div class=&quot;reveal&quot; id=&quot;animatedModal10&quot; data-reveal data-close-on-click=&quot;true&quot; data-animation-in=&quot;spin-in&quot; data-animation-out=&quot;spin-out&quot;&gt;
                            &lt;h1&gt;Whoa, I'm dizzy!&lt;/h1&gt;
                            &lt;p class=&quot;lead&quot;&gt;There are many options for animating modals, check out the Motion UI library to see them all&lt;/p&gt;
                            &lt;button class=&quot;close-button&quot; data-close aria-label=&quot;Close reveal&quot; type=&quot;button&quot;&gt;
                                &lt;span aria-hidden=&quot;true&quot;&gt;&amp;times;&lt;/span&gt;
                            &lt;/button&gt;
                    </code>
                 </pre>
            </div>
        </div>
    </div>
</div>




<script>
    Prism.highlightAll();
    CallCopyToClipboard();

    $(document).ready(function () {
        $(".flip-reveal-html").click(function () {
            $(".panel-reveal-html").slideToggle("slow");
        });
    });
</script>