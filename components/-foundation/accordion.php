<div class="grid-x master animated fadeIn">
    <div class="cell small-12">
        <div class="cell-margin">

            <ul class="accordion" data-accordion>
                <li class="accordion-item is-active" data-accordion-item>
                    <a href="#" class="accordion-title">Accordion 1</a>
                    <div class="accordion-content" data-tab-content >
                        <p>E-mailadress</p>
                        <a href="#">Nowhere to Go</a>
                    </div>
                </li>

                <li class="accordion-item" data-accordion-item>
                    <a href="#" class="accordion-title">Accordion 2</a>
                    <div class="accordion-content" data-tab-content>
                        <textarea title=""></textarea>
                        <button class="button">I do nothing!</button>
                    </div>
                </li>

                <li class="accordion-item" data-accordion-item>
                    <a href="#" class="accordion-title">Accordion 3</a>
                    <div class="accordion-content" data-tab-content>
                        Type your name!
                        <input type="text" title="">
                    </div>
                </li>
            </ul>

        </div>
    </div>
    <div class="cell small-12">
        <div class="cell-margin">

            <h1 class="title">
                Accordion
            </h1>

<!--            <div class="component-buttons">-->
<!--                <button type="button" class="flip arc-flip show-code-html"><i class="fas fa-code"></i> Html</button>-->
<!--                <button type="button" onClick="CopyToClipboard('to-copy')" id="copied">-->
<!--                    <i class="far fa-copy"></i> Copy</button>-->
<!--            </div>-->



    <pre class="line-numbers language-markup">
    <code class="language-markup">&lt;ul class=&quot;accordion&quot; data-accordion&gt;
        &lt;li class=&quot;accordion-item is-active&quot; data-accordion-item&gt;
            &lt;a href=&quot;#&quot; class=&quot;accordion-title&quot;&gt;Accordion 1&lt;/a&gt;
            &lt;div class=&quot;accordion-content&quot; data-tab-content &gt;
                &lt;p&gt;Panel 1. Lorem ipsum dolor&lt;/p&gt;
                &lt;a href=&quot;#&quot;&gt;Nowhere to Go&lt;/a&gt;
            &lt;/div&gt;
        &lt;/li&gt;

        &lt;li class=&quot;accordion-item&quot; data-accordion-item&gt;
            &lt;a href=&quot;#&quot; class=&quot;accordion-title&quot;&gt;Accordion 2&lt;/a&gt;
            &lt;div class=&quot;accordion-content&quot; data-tab-content&gt;
                &lt;textarea title=&quot;&quot;&gt;&lt;/textarea&gt;
                &lt;button class=&quot;button&quot;&gt;I do nothing!&lt;/button&gt;
            &lt;/div&gt;
        &lt;/li&gt;

        &lt;li class=&quot;accordion-item&quot; data-accordion-item&gt;
            &lt;a href=&quot;#&quot; class=&quot;accordion-title&quot;&gt;Accordion 3&lt;/a&gt;
            &lt;div class=&quot;accordion-content&quot; data-tab-content&gt;
                Type your name!
                &lt;input type=&quot;text&quot; title=&quot;&quot;&gt;
            &lt;/div&gt;
        &lt;/li&gt;
    &lt;/ul&gt;</code><button class="copy-button">Copy</button>
    </pre>

        </div>
    </div>
</div>




<script>
    Prism.highlightAll();
    CallCopyToClipboard();

    $(document).ready(function() {
        $(".arc-flip").click(function() {
            $(".arc-panel").slideToggle("slow");
        });
    });
</script>