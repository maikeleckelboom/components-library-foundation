<section class="window">
<!--    <p class="margin-left"><a href="https://foundation.zurb.com/sites/docs/dropdown.html">Documentatie</a></p>-->

    <button class="button" type="button" data-toggle="example-dropdown">Toggle Dropdown</button>
    <div class="dropdown-pane" id="example-dropdown" data-dropdown data-auto-focus="true">
        Example form in a dropdown.
        <form>
            <div class="grid-container">
                <div class="grid-x grid-margin-x">
                    <div class="cell medium-6">
                        <label>Name
                            <input type="text" placeholder="Kirk, James T.">
                        </label>
                    </div>
                    <div class="cell medium-6">
                        <label>Rank
                            <input type="text" placeholder="Captain">
                        </label>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <button class="button" type="button" data-toggle="example-dropdown-1">Hoverable Dropdown</button>
    <div class="dropdown-pane" id="example-dropdown-1" data-dropdown data-hover="true" data-hover-pane="true">
        Just some junk that needs to be said. Or not. Your choice.
    </div>


    <div class="button-group">
        <button class="flip button show-code flip-dropdown">Show Code</button>
        <button onClick="CopyToClipboard('to-copy')" class="button" id="copied">
            Copy to clipboard
        </button>
    </div>

    <div class="panel panel-dropdown" id="to-copy">
        <pre class="line-numbers language-markup">
            <code id="to-copy">&lt;button class=&quot;button&quot; type=&quot;button&quot; data-toggle=&quot;example-dropdown&quot;&gt;Toggle Dropdown&lt;/button&gt;
            &lt;div class=&quot;dropdown-pane&quot; id=&quot;example-dropdown&quot; data-dropdown data-auto-focus=&quot;true&quot;&gt;
                Example form in a dropdown.
                &lt;form&gt;
                    &lt;div class=&quot;grid-container&quot;&gt;
                        &lt;div class=&quot;grid-x grid-margin-x&quot;&gt;
                            &lt;div class=&quot;cell medium-6&quot;&gt;
                                &lt;label&gt;Name
                                    &lt;input type=&quot;text&quot; placeholder=&quot;Kirk, James T.&quot;&gt;
                                &lt;/label&gt;
                            &lt;/div&gt;
                            &lt;div class=&quot;cell medium-6&quot;&gt;
                                &lt;label&gt;Rank
                                    &lt;input type=&quot;text&quot; placeholder=&quot;Captain&quot;&gt;
                                &lt;/label&gt;
                            &lt;/div&gt;
                        &lt;/div&gt;
                    &lt;/div&gt;
                &lt;/form&gt;
            &lt;/div&gt;

            &lt;button class=&quot;button&quot; type=&quot;button&quot; data-toggle=&quot;example-dropdown-1&quot;&gt;Hoverable Dropdown&lt;/button&gt;
            &lt;div class=&quot;dropdown-pane&quot; id=&quot;example-dropdown-1&quot; data-dropdown data-hover=&quot;true&quot; data-hover-pane=&quot;true&quot;&gt;
                Just some junk that needs to be said. Or not. Your choice.
            &lt;/div&gt;</code>
        </pre>
    </div>



</section>
<script>
    Prism.highlightAll();
    CallCopyToClipboard();

    $(document).ready(function () {
        $(".flip-dropdown").click(function () {
            $(".panel-dropdown").slideToggle("slow");
        });
    });
</script>