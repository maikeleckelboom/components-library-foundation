v<div class="grid-x master animated fadeIn">

    <div class="cell small-24 medium-11 overflow">
        <div class="cell-margin">

            <!--Primary-->
            <div class="callout primary" data-closable>
                <h5>This is a primay callout</h5>
                <p>It has an easy to override visual style, and is appropriately subdued.</p>
                <a href="#">It's dangerous to go alone, take this.</a>

                <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <!--Secondary-->
            <div class="callout secondary" data-closable>
                <h5>This is a secondary callout</h5>
                <p>It has an easy to override visual style, and is appropriately subdued.</p>
                <a href="#">It's dangerous to go alone, take this.</a>
                <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <!--Success-->
            <div class="callout success" data-closable>
                <h5>This is a success callout</h5>
                <p>It has an easy to override visual style, and is appropriately subdued.</p>
                <a href="#">It's dangerous to go alone, take this.</a>
                <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <!--Warning-->
            <div class="callout warning" data-closable>
                <h5>This is a warning callout</h5>
                <p>It has an easy to override visual style, and is appropriately subdued.</p>
                <a href="#">It's dangerous to go alone, take this.</a>
                <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <!--Alert-->
            <div class="callout alert" data-closable>
                <h5>This is a alert callout</h5>
                <p>It has an easy to override visual style, and is appropriately subdued.</p>
                <a href="#">It's dangerous to go alone, take this.</a>
                <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    </div>



    <div class="cell small-24 medium-13 overflow">
        <div class="cell-margin">

            <h1 class="title">Call Out</h1>


<!--<span>Foundation <i class="fal fa-star-of-life"></i></span>-->

            <!--HTMl5-->
            <div class="component-buttons">
<!--                <button type="button" class="flip flip-call-html show-code-html"><i class="fab fa-html5"></i> Html</button>-->
<!--                <button type="button" onClick="CopyToClipboard('to-copy')" id="copied">-->
<!--                    <i class="far fa-copy"> Copy</i></button>-->
            </div>


            <!--Panel HTML5-->
<!--            <div class="panel panel-call-html" id="to-copy">-->
                <pre class="line-numbers language-markup"><button class="copy-button">Copy</button>
                    <code id="to-copy">&lt;!--Primary--&gt;
                    &lt;div class=&quot;callout primary&quot; data-closable&gt;
                        &lt;h5&gt;This is a primay callout&lt;/h5&gt;
                        &lt;p&gt;It has an easy to override visual style, and is appropriately subdued.&lt;/p&gt;
                        &lt;a href=&quot;#&quot;&gt;It's dangerous to go alone, take this.&lt;/a&gt;

                        &lt;button class=&quot;close-button&quot; aria-label=&quot;Dismiss alert&quot; type=&quot;button&quot; data-close&gt;
                            &lt;span aria-hidden=&quot;true&quot;&gt;&amp;times;&lt;/span&gt;
                        &lt;/button&gt;
                    &lt;/div&gt;</code>
                </pre>
<!--            </div>-->
<!--        </div>-->

        <!--Foundation Required-->



    </div>
<script>
    Prism.highlightAll();
    CallCopyToClipboard();

    $(document).ready(function () {
        $(".flip-call-html").click(function () {
            $(".panel-call-html").slideToggle("slow");
        });
    });

    $(document).ready(function () {
        $(".flip-call-scss").click(function () {
            $(".panel-call-scss").slideToggle("slow");
        });
    });
</script>