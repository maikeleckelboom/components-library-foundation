<div class="grid-x master animated fadeIn">

    <div class="cell small-24 medium-11 overflow">
        <div class="cell-margin">

    <div class="callout" data-closable>
        <p>You can so totally close this!</p>
        <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
            <span aria-hidden="true">&times;</span>
        </button>
    </div>

    <div class="success callout" data-closable="slide-out-right">
        <p>You can close me too, and I close using a Motion UI animation.</p>
        <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
            <span aria-hidden="true">&times;</span>
        </button>
    </div>

        </div>
    </div>

    <div class="cell small-24 medium-13 overflow">
        <div class="cell-margin">

            <h1 class="title">
                Close Button + Call Out
            </h1>

            <div class="component-buttons">
                <button type="button" class="flip flip-close-html show-code-html"><i class="fab fa-html5"></i> Html</button>
                <button type="button" onClick="CopyToClipboard('to-copy')" id="copied">
                    <i class="far fa-copy"> Copy</i></button>
            </div>

            <div class="panel panel-close-html" id="to-copy">
        <pre class="line-numbers language-markup">
            <code id="to-copy">   &lt;div class=&quot;callout&quot; data-closable&gt;
                &lt;p&gt;You can so totally close this!&lt;/p&gt;
                &lt;button class=&quot;close-button&quot; aria-label=&quot;Dismiss alert&quot; type=&quot;button&quot; data-close&gt;
                &lt;span aria-hidden=&quot;true&quot;&gt;&amp;times;&lt;/span&gt;
                &lt;/button&gt;
                &lt;/div&gt;

                &lt;div class=&quot;success callout&quot; data-closable=&quot;slide-out-right&quot;&gt;
                &lt;p&gt;You can close me too, and I close using a Motion UI animation.&lt;/p&gt;
                &lt;button class=&quot;close-button&quot; aria-label=&quot;Dismiss alert&quot; type=&quot;button&quot; data-close&gt;
                &lt;span aria-hidden=&quot;true&quot;&gt;&amp;times;&lt;/span&gt;
                &lt;/button&gt;
                &lt;/div&gt;</code>
        </pre>
            </div>
        </div>
    </div>
</div>


<script>
    Prism.highlightAll();
    CallCopyToClipboard();

    $(document).ready(function () {
        $(".flip-close-html").click(function () {
            $(".panel-close-html").slideToggle("slow");
        });
    });
</script>