<div class="grid-x master animated fadeIn">
    <div class="cell small-12">
        <div class="cell-margin">

        </div>
    </div>
    <div class="cell small-12">
        <div class="cell-margin">

        </div>
    </div>
</div>

<!-- Active button-->
<button class="button">Button</button>


<div class="button-group">
    <a class="button">Small</a>
    <a class="button">Button</a>
    <a class="button">Group</a>
</div>

<!--Small button group-->
<div class="small button-group">
    <a class="button">Small</a>
    <a class="button">Button</a>
    <a class="button">Group</a>
</div>

<!--Button Colors-->
<div class="button-group">
    <a class="secondary button">View</a>
    <a class="success button">Edit</a>
    <a class="warning button">Share</a>
    <a class="alert button">Delete</a>
</div>

<!--Secondary buttons-->
<div class="secondary button-group">
    <a class="button">Harder</a>
    <a class="button">Better</a>
    <a class="button">Faster</a>
    <a class="button">Stronger</a>
</div>

<!--Expanded button group -->
<div class="expanded button-group">
    <a class="button">Expanded</a>
    <a class="button">Button</a>
    <a class="button">Group</a>
</div>

<!--Stacked-->
<div class="stacked-for-small button-group">
    <a class="button">How</a>
    <a class="button">Low</a>
    <a class="button">Can</a>
    <a class="button">You</a>
    <a class="button">Go</a>
</div>

<!--Split Button-->
<div class="button-group">
    <a class="button">Primary Action</a>
    <a class="dropdown button arrow-only">
        <span class="show-for-sr">Show menu</span>
    </a>
</div>


</section>
