<section class="window">

    <!-- Add a Menu -->
    <ul class="menu" data-magellan>
        <li><a href="#first">First Arrival</a></li>
        <li><a href="#second">Second Arrival</a></li>
        <li><a href="#third">Third Arrival</a></li>
        <li><a href="#fourth">Fourth Arrival</a></li>
        <li><a href="#fifth">Fifth Arrival</a></li>
    </ul>


    <!-- Add content where magellan will be linked -->
    <div class="grid-x">
        <h1 id="first" data-magellan-target="first"></h1>
    </div>

    <div class="grid-x">
        <h1 id="second" data-magellan-target="second"></h1>
    </div>

    <div class="grid-x">
        <h1 id="third" data-magellan-target="third"></h1>
    </div>

    <div class="grid-x">
        <h1 id="fourth" data-magellan-target="fourth"></h1>
    </div>

    <div class="grid-x">
        <h1 id="fifth" data-magellan-target="fourth"></h1>
    </div>


    <div class="button-group">
        <button class="flip button show-code flip-magellan">Show Code</button>
        <button onClick="CopyToClipboard('to-copy')" class="button" id="copied">
            Copy to clipboard
        </button>
    </div>

    <div class="panel panel-magellan" id="to-copy">
        <pre class="line-numbers language-markup">
            <code id="to-copy">&lt;!-- Add a Menu --&gt;
            &lt;ul class=&quot;menu&quot; data-magellan&gt;
                &lt;li&gt;&lt;a href=&quot;#first&quot;&gt;First Arrival&lt;/a&gt;&lt;/li&gt;
                &lt;li&gt;&lt;a href=&quot;#second&quot;&gt;Second Arrival&lt;/a&gt;&lt;/li&gt;
                &lt;li&gt;&lt;a href=&quot;#third&quot;&gt;Third Arrival&lt;/a&gt;&lt;/li&gt;
                &lt;li&gt;&lt;a href=&quot;#fourth&quot;&gt;Fourth Arrival&lt;/a&gt;&lt;/li&gt;
                &lt;li&gt;&lt;a href=&quot;#fifth&quot;&gt;Fifth Arrival&lt;/a&gt;&lt;/li&gt;
            &lt;/ul&gt;


            &lt;!-- Add content where magellan will be linked --&gt;
            &lt;div class=&quot;grid-x&quot;&gt;
                &lt;h1 id=&quot;first&quot; data-magellan-target=&quot;first&quot;&gt;&lt;/h1&gt;
            &lt;/div&gt;

            &lt;div class=&quot;grid-x&quot;&gt;
                &lt;h1 id=&quot;second&quot; data-magellan-target=&quot;second&quot;&gt;&lt;/h1&gt;
            &lt;/div&gt;

            &lt;div class=&quot;grid-x&quot;&gt;
                &lt;h1 id=&quot;third&quot; data-magellan-target=&quot;third&quot;&gt;&lt;/h1&gt;
            &lt;/div&gt;

            &lt;div class=&quot;grid-x&quot;&gt;
                &lt;h1 id=&quot;fourth&quot; data-magellan-target=&quot;fourth&quot;&gt;&lt;/h1&gt;
            &lt;/div&gt;

            &lt;div class=&quot;grid-x&quot;&gt;
                &lt;h1 id=&quot;fifth&quot; data-magellan-target=&quot;fourth&quot;&gt;&lt;/h1&gt;
            &lt;/div&gt;</code>
        </pre>
    </div>




</section>
<script>
    Prism.highlightAll();
    CallCopyToClipboard();

    $(document).ready(function () {
        $(".flip-magellan").click(function () {
            $(".panel-magellan").slideToggle("slow");
        });
    });
</script>