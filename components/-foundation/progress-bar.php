<div class="grid-x master animated fadeIn">

    <div class="cell small-24 medium-11 overflow">
        <div class="cell-margin">

            <!--Default-->
            <div class="progress" role="progressbar" tabindex="0" aria-valuenow="25" aria-valuemin="0"
                 aria-valuetext="95 percent" aria-valuemax="100">
      <span class="progress-meter" style="width: 95%">
        <p class="progress-meter-text">95%</p>
      </span>
            </div>

            <!-- Secondary -->
            <div class="progress secondary" role="progressbar" tabindex="0" aria-valuenow="25" aria-valuemin="0"
                 aria-valuetext="85 percent" aria-valuemax="100">
      <span class="progress-meter" style="width: 85%">
        <p class="progress-meter-text">85%</p>
      </span>
            </div>

            <!-- Success -->
            <div class="progress success" role="progressbar" tabindex="0" aria-valuenow="25" aria-valuemin="0"
                 aria-valuetext="75 percent" aria-valuemax="100">
      <span class="progress-meter" style="width: 75%">
        <p class="progress-meter-text">75%</p>
      </span>
            </div>

            <!-- Warning -->
            <div class="progress warning" role="progressbar" tabindex="0" aria-valuenow="25" aria-valuemin="0"
                 aria-valuetext="50 percent" aria-valuemax="100">
      <span class="progress-meter" style="width: 50%">
        <p class="progress-meter-text">50%</p>
      </span>
            </div>

            <!--  Alert  -->
            <div class="progress alert" role="progressbar" tabindex="0" aria-valuenow="25" aria-valuemin="0"
                 aria-valuetext="25 percent" aria-valuemax="100">
      <span class="progress-meter" style="width: 25%">
        <p class="progress-meter-text">25%</p>
      </span>
            </div>
        </div>
    </div>


    <div class="cell small-24 medium-13 overflow">
        <div class="cell-margin">

            <h1 class="title">
                Progress Bar
            </h1>

<!---->
<!--                <p>-->
<!--                    <a href="https://foundation.zurb.com/sites/docs/progress-bar.html">-->
<!--                        Documentatie-->
<!--                    </a>-->
<!--                </p>-->

            <div class="component-buttons">

                <button type="button" class="flip flip-progress-html show-code-html"><i class="fab fa-html5"></i> Html</button>

                <button type="button" onClick="CopyToClipboard('to-copy')" id="copied">
                    <i class="far fa-copy"></i> Copy</button>

            </div>


            <div class="panel panel-progress-html" id="to-copy">
                <pre class="line-numbers language-markup">
                    <code id="to-copy">&lt;!--Default--&gt;
                    &lt;div class=&quot;progress&quot; role=&quot;progressbar&quot; tabindex=&quot;0&quot; aria-valuenow=&quot;25&quot; aria-valuemin=&quot;0&quot; aria-valuetext=&quot;95 percent&quot; aria-valuemax=&quot;100&quot;&gt;
                      &lt;span class=&quot;progress-meter&quot; style=&quot;width: 95%&quot;&gt;
                        &lt;p class=&quot;progress-meter-text&quot;&gt;95%&lt;/p&gt;
                      &lt;/span&gt;
                    &lt;/div&gt;

                    &lt;!-- Secondary --&gt;
                    &lt;div class=&quot;progress secondary&quot; role=&quot;progressbar&quot; tabindex=&quot;0&quot; aria-valuenow=&quot;25&quot; aria-valuemin=&quot;0&quot; aria-valuetext=&quot;85 percent&quot; aria-valuemax=&quot;100&quot;&gt;
                      &lt;span class=&quot;progress-meter&quot; style=&quot;width: 85%&quot;&gt;
                        &lt;p class=&quot;progress-meter-text&quot;&gt;85%&lt;/p&gt;
                      &lt;/span&gt;
                    &lt;/div&gt;

                    &lt;!-- Success --&gt;
                    &lt;div class=&quot;progress success&quot; role=&quot;progressbar&quot; tabindex=&quot;0&quot; aria-valuenow=&quot;25&quot; aria-valuemin=&quot;0&quot; aria-valuetext=&quot;75 percent&quot; aria-valuemax=&quot;100&quot;&gt;
                      &lt;span class=&quot;progress-meter&quot; style=&quot;width: 75%&quot;&gt;
                        &lt;p class=&quot;progress-meter-text&quot;&gt;75%&lt;/p&gt;
                      &lt;/span&gt;
                    &lt;/div&gt;

                    &lt;!-- Warning --&gt;
                    &lt;div class=&quot;progress warning&quot; role=&quot;progressbar&quot; tabindex=&quot;0&quot; aria-valuenow=&quot;25&quot; aria-valuemin=&quot;0&quot; aria-valuetext=&quot;50 percent&quot; aria-valuemax=&quot;100&quot;&gt;
                      &lt;span class=&quot;progress-meter&quot; style=&quot;width: 50%&quot;&gt;
                        &lt;p class=&quot;progress-meter-text&quot;&gt;50%&lt;/p&gt;
                      &lt;/span&gt;
                    &lt;/div&gt;

                    &lt;!--  Alert  --&gt;
                    &lt;div class=&quot;progress alert&quot; role=&quot;progressbar&quot; tabindex=&quot;0&quot; aria-valuenow=&quot;25&quot; aria-valuemin=&quot;0&quot; aria-valuetext=&quot;25 percent&quot; aria-valuemax=&quot;100&quot;&gt;
                      &lt;span class=&quot;progress-meter&quot; style=&quot;width: 25%&quot;&gt;
                        &lt;p class=&quot;progress-meter-text&quot;&gt;25%&lt;/p&gt;
                      &lt;/span&gt;
                    &lt;/div&gt;


                    &lt;div class=&quot;button-group&quot;&gt;
                        &lt;button class=&quot;flip button show-code flip-progress&quot;&gt;Show Code&lt;/button&gt;
                        &lt;button onClick=&quot;CopyToClipboard('to-copy')&quot; class=&quot;button&quot; id=&quot;copied&quot;&gt;
                            Copy to clipboard
                        &lt;/button&gt;
                    &lt;/div&gt;</code>
                </pre>
            </div>
        </div>
    </div>
</div>


<script>
    Prism.highlightAll();
    CallCopyToClipboard();

    $(document).ready(function () {
        $(".flip-progress-html").click(function () {
            $(".panel-progress-html").slideToggle("slow");
        });
    });

</script>