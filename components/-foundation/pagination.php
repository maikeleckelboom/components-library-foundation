<div class="grid-x master animated fadeIn">

    <div class="cell small-24 medium-11 overflow">
        <div class="cell-margin">
            <nav aria-label="Pagination">
                <ul class="pagination">
                    <li class="pagination-previous disabled">Vorige <span class="show-for-sr">page</span></li>
                    <li class="current"><span class="show-for-sr">Pagina</span> 1</li>
                    <li><a href="#" aria-label="Page 2">2</a></li>
                    <li><a href="#" aria-label="Page 3">3</a></li>
                    <li><a href="#" aria-label="Page 4">4</a></li>
                    <li class="ellipsis" aria-hidden="true"></li>
                    <li><a href="#" aria-label="Page 12">12</a></li>
                    <li><a href="#" aria-label="Page 13">13</a></li>
                    <li class="pagination-next"><a href="#" aria-label="Next page">Volgende <span class="show-for-sr">pagina</span></a></li>
                </ul>
            </nav>
        </div>
    </div>

    <div class="cell small-24 medium-13 overflow">
        <div class="cell-margin">

            <div class="component-buttons">

                <button type="button" class="flip flip-pagination-html show-code-html"><i class="fab fa-html5"></i> Html</button>

                <button type="button" onClick="CopyToClipboard('to-copy')" id="copied">
                    <i class="far fa-copy"></i> Copy</button>

            </div>

            <div class="panel panel-pagination-html" id="to-copy">
                <pre class="line-numbers language-markup">
                    <code id="to-copy">&lt;nav aria-label=&quot;Pagination&quot;&gt;
                        &lt;ul class=&quot;pagination&quot;&gt;
                            &lt;li class=&quot;pagination-previous disabled&quot;&gt;Previous &lt;span class=&quot;show-for-sr&quot;&gt;page&lt;/span&gt;&lt;/li&gt;
                            &lt;li class=&quot;current&quot;&gt;&lt;span class=&quot;show-for-sr&quot;&gt;You're on page&lt;/span&gt; 1&lt;/li&gt;
                            &lt;li&gt;&lt;a href=&quot;#&quot; aria-label=&quot;Page 2&quot;&gt;2&lt;/a&gt;&lt;/li&gt;
                            &lt;li&gt;&lt;a href=&quot;#&quot; aria-label=&quot;Page 3&quot;&gt;3&lt;/a&gt;&lt;/li&gt;
                            &lt;li&gt;&lt;a href=&quot;#&quot; aria-label=&quot;Page 4&quot;&gt;4&lt;/a&gt;&lt;/li&gt;
                            &lt;li class=&quot;ellipsis&quot; aria-hidden=&quot;true&quot;&gt;&lt;/li&gt;
                            &lt;li&gt;&lt;a href=&quot;#&quot; aria-label=&quot;Page 12&quot;&gt;12&lt;/a&gt;&lt;/li&gt;
                            &lt;li&gt;&lt;a href=&quot;#&quot; aria-label=&quot;Page 13&quot;&gt;13&lt;/a&gt;&lt;/li&gt;
                            &lt;li class=&quot;pagination-next&quot;&gt;&lt;a href=&quot;#&quot; aria-label=&quot;Next page&quot;&gt;Next &lt;span class=&quot;show-for-sr&quot;&gt;page&lt;/span&gt;&lt;/a&gt;&lt;/li&gt;
                        &lt;/ul&gt;
                    &lt;/nav&gt;</code>
                </pre>
            </div>
        </div>
    </div>
</div>






<script>
    Prism.highlightAll();
    CallCopyToClipboard();

    $(document).ready(function () {
        $(".flip-pagination-html").click(function () {
            $(".panel-pagination-html").slideToggle("slow");
        });
    });
</script>
