<div class="grid-x animated fadeIn">
    <div class="cell small-12">
        <div class="cell-margin">

            <table class="hover unstriped">
                <thead>
                <tr>
                    <th width="200">Table Header</th>
                    <th>Table Header</th>
                    <th width="150">Table Header</th>
                    <th width="150">Table Header</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Content Goes Here</td>
                    <td>This is longer content Donec id elit non mi porta gravida at eget metus.</td>
                    <td>Content Goes Here</td>
                    <td>Content Goes Here</td>
                </tr>
                <tr>
                    <td>Content Goes Here</td>
                    <td>This is longer Content Goes Here Donec id elit non mi porta gravida at eget metus.</td>
                    <td>Content Goes Here</td>
                    <td>Content Goes Here</td>
                </tr>
                <tr>
                    <td>Content Goes Here</td>
                    <td>This is longer Content Goes Here Donec id elit non mi porta gravida at eget metus.</td>
                    <td>Content Goes Here</td>
                    <td>Content Goes Here</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="cell small-12">
        <div class="cell-margin">
            <div class="button-group">
                <button class="flip button show-code flip-table">Show Code</button>
                <button onClick="CopyToClipboard('to-copy')" class="button" id="copied">
                    Copy to clipboard
                </button>
            </div>

            <div class="panel panel-table" id="to-copy">
        <pre class="line-numbers language-markup">
            <code id="to-copy">&lt;table class=&quot;hover unstriped&quot;&gt;
                &lt;thead&gt;
                &lt;tr&gt;
                    &lt;th width=&quot;200&quot;&gt;Table Header&lt;/th&gt;
                    &lt;th&gt;Table Header&lt;/th&gt;
                    &lt;th width=&quot;150&quot;&gt;Table Header&lt;/th&gt;
                    &lt;th width=&quot;150&quot;&gt;Table Header&lt;/th&gt;
                &lt;/tr&gt;
                &lt;/thead&gt;
                &lt;tbody&gt;
                &lt;tr&gt;
                    &lt;td&gt;Content Goes Here&lt;/td&gt;
                    &lt;td&gt;This is longer content Donec id elit non mi porta gravida at eget metus.&lt;/td&gt;
                    &lt;td&gt;Content Goes Here&lt;/td&gt;
                    &lt;td&gt;Content Goes Here&lt;/td&gt;
                &lt;/tr&gt;
                &lt;tr&gt;
                    &lt;td&gt;Content Goes Here&lt;/td&gt;
                    &lt;td&gt;This is longer Content Goes Here Donec id elit non mi porta gravida at eget metus.&lt;/td&gt;
                    &lt;td&gt;Content Goes Here&lt;/td&gt;
                    &lt;td&gt;Content Goes Here&lt;/td&gt;
                &lt;/tr&gt;
                &lt;tr&gt;
                    &lt;td&gt;Content Goes Here&lt;/td&gt;
                    &lt;td&gt;This is longer Content Goes Here Donec id elit non mi porta gravida at eget metus.&lt;/td&gt;
                    &lt;td&gt;Content Goes Here&lt;/td&gt;
                    &lt;td&gt;Content Goes Here&lt;/td&gt;
                &lt;/tr&gt;
                &lt;/tbody&gt;
            &lt;/table&gt;</code>
        </pre>
            </div>
        </div>
    </div>
</div>







</section>
<script>
    Prism.highlightAll();
    CallCopyToClipboard();

    $(document).ready(function () {
        $(".flip-table").click(function () {
            $(".panel-table").slideToggle("slow");
        });
    });
</script>