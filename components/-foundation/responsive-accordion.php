<div class="grid-x master animated fadeIn">

    <div class="cell small-24 medium-11 overflow">
        <div class="cell-margin">

            <ul class="tabs" data-responsive-accordion-tabs="tabs small-accordion medium-tabs" id="example-tabs">
                <li class="tabs-title is-active"><a href="#panel1" aria-selected="true">Tab 1</a></li>
                <li class="tabs-title"><a href="#panel2">Tab 2</a></li>
                <li class="tabs-title"><a href="#panel3">Tab 3</a></li>
                <li class="tabs-title"><a href="#panel4">Tab 4</a></li>
                <li class="tabs-title"><a href="#panel5">Tab 5</a></li>
                <li class="tabs-title"><a href="#panel6">Tab 6</a></li>
            </ul>

            <div class="tabs-content" data-tabs-content="example-tabs">
                <div class="tabs-panel is-active" id="panel1">
                    <p>one</p>
                    <p>Check me out! I'm a super cool Tab panel with text content!</p>
                </div>

                <div class="tabs-panel" id="panel2">
                    <p>two</p>
                    <img class="thumbnail" src="images/firewatch_150305_06.png">
                </div>

                <div class="tabs-panel" id="panel3">
                    <p>three</p>
                    <p>Check me out! I'm a super cool Tab panel with text content!</p>
                </div>

                <div class="tabs-panel" id="panel4">
                    <p>four</p>
                    <img class="thumbnail" src="images/factif.png">
                </div>

                <div class="tabs-panel" id="panel5">
                    <p>five</p>
                    <p>Check me out! I'm a super cool Tab panel with text content!</p>
                </div>

                <div class="tabs-panel" id="panel6">
                    <p>six</p>
                    <img class="thumbnail" src="images/atomic-land.jpeg">
                </div>
            </div>


        </div>
    </div>

    <div class="cell small-24 medium-13 overflow">
        <div class="cell-margin">

            <h1 class="title">Responsive Accordion</h1>

            <!--HTMl5-->
            <div class="component-buttons">
                <button type="button" class="flip flip-r-acc-html show-code-html"><i class="fab fa-html5"></i> Html</button>
                <button type="button" onClick="CopyToClipboard('to-copy')" id="copied">
                    <i class="far fa-copy"> Copy</i></button>
            </div>

            <div class="panel panel-r-acc-html" id="to-copy">
                <pre class="line-numbers language-markup">
                    <code id="to-copy">&lt;ul class=&quot;tabs&quot; data-responsive-accordion-tabs=&quot;tabs small-accordion medium-tabs&quot; id=&quot;example-tabs&quot;&gt;
                        &lt;li class=&quot;tabs-title is-active&quot;&gt;&lt;a href=&quot;#panel1&quot; aria-selected=&quot;true&quot;&gt;Tab 1&lt;/a&gt;&lt;/li&gt;
                        &lt;li class=&quot;tabs-title&quot;&gt;&lt;a href=&quot;#panel2&quot;&gt;Tab 2&lt;/a&gt;&lt;/li&gt;
                        &lt;li class=&quot;tabs-title&quot;&gt;&lt;a href=&quot;#panel3&quot;&gt;Tab 3&lt;/a&gt;&lt;/li&gt;
                        &lt;li class=&quot;tabs-title&quot;&gt;&lt;a href=&quot;#panel4&quot;&gt;Tab 4&lt;/a&gt;&lt;/li&gt;
                        &lt;li class=&quot;tabs-title&quot;&gt;&lt;a href=&quot;#panel5&quot;&gt;Tab 5&lt;/a&gt;&lt;/li&gt;
                        &lt;li class=&quot;tabs-title&quot;&gt;&lt;a href=&quot;#panel6&quot;&gt;Tab 6&lt;/a&gt;&lt;/li&gt;
                    &lt;/ul&gt;

                    &lt;div class=&quot;tabs-content&quot; data-tabs-content=&quot;example-tabs&quot;&gt;
                        &lt;div class=&quot;tabs-panel is-active&quot; id=&quot;panel1&quot;&gt;
                            &lt;p&gt;one&lt;/p&gt;
                            &lt;p&gt;Check me out! I'm a super cool Tab panel with text content!&lt;/p&gt;
                        &lt;/div&gt;

                        &lt;div class=&quot;tabs-panel&quot; id=&quot;panel2&quot;&gt;
                            &lt;p&gt;two&lt;/p&gt;
                            &lt;img class=&quot;thumbnail&quot; src=&quot;images/firewatch_150305_06.png&quot;&gt;
                        &lt;/div&gt;

                        &lt;div class=&quot;tabs-panel&quot; id=&quot;panel3&quot;&gt;
                            &lt;p&gt;three&lt;/p&gt;
                            &lt;p&gt;Check me out! I'm a super cool Tab panel with text content!&lt;/p&gt;
                        &lt;/div&gt;

                        &lt;div class=&quot;tabs-panel&quot; id=&quot;panel4&quot;&gt;
                            &lt;p&gt;four&lt;/p&gt;
                            &lt;img class=&quot;thumbnail&quot; src=&quot;images/factif.png&quot;&gt;
                        &lt;/div&gt;

                        &lt;div class=&quot;tabs-panel&quot; id=&quot;panel5&quot;&gt;
                            &lt;p&gt;five&lt;/p&gt;
                            &lt;p&gt;Check me out! I'm a super cool Tab panel with text content!&lt;/p&gt;
                        &lt;/div&gt;

                        &lt;div class=&quot;tabs-panel&quot; id=&quot;panel6&quot;&gt;
                            &lt;p&gt;six&lt;/p&gt;
                            &lt;img class=&quot;thumbnail&quot; src=&quot;images/atomic-land.jpeg&quot;&gt;
                        &lt;/div&gt;
                    &lt;/div&gt;</code>
                </pre>
            </div>

        </div>
    </div>

</div>



<script>
    Prism.highlightAll();
    CallCopyToClipboard();

    $(document).ready(function () {
        $(".flip-r-acc-html").click(function () {
            $(".panel-r-acc-html").slideToggle("slow");
        });
    });
</script>