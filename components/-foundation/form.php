<div class="grid-x master">

    <div class="cell small-24 medium-12">
        <div class="cell-margin">


    <!--Text inputs-->
    <form>
        <label>Input Label
            <input type="text" placeholder="">
        </label>
    </form>

    <!--Number inputs-->
    <label>
        How many puppies?
        <input type="number" value="100">
    </label>

    <!--Text areas-->
    <label>
        What books did you read over summer break?
        <textarea placeholder="None"></textarea>
    </label>


    <!--Select Menus-->
    <label>Select Menu
        <select>
            <option value="husker">Husker</option>
            <option value="starbuck">Starbuck</option>
            <option value="hotdog">Hot Dog</option>
            <option value="apollo">Apollo</option>
        </select>
    </label>

    <!--Multiple Selections-->
    <label>Select Menu - Multiple
        <select multiple>
            <option value="husker">Husker</option>
            <option value="starbuck">Starbuck</option>
            <option value="hotdog">Hot Dog</option>
            <option value="apollo">Apollo</option>
        </select>
    </label>

    <!--Radio buttons-->
    <div class="grid-x grid-padding-x">
        <fieldset class="large-5 cell">
            <legend>Choose Your Favorite</legend>
            <input type="radio" name="pokemon" value="Red" id="pokemonRed" required><label for="pokemonRed">Red</label>
            <input type="radio" name="pokemon" value="Blue" id="pokemonBlue"><label for="pokemonBlue">Blue</label>
            <input type="radio" name="pokemon" value="Yellow" id="pokemonYellow"><label
                    for="pokemonYellow">Yellow</label>
        </fieldset>

        <!--Checkboxes-->
        <fieldset class="large-7 cell">
            <legend>Check these out</legend>
            <input id="checkbox1" type="checkbox"><label for="checkbox1">Checkbox 1</label>
            <input id="checkbox2" type="checkbox"><label for="checkbox2">Checkbox 2</label>
            <input id="checkbox3" type="checkbox"><label for="checkbox3">Checkbox 3</label>
        </fieldset>
    </div>


    <!--Fieldset styles-->
    <fieldset class="fieldset">
        <legend>Check these out</legend>
        <input id="checkbox12" type="checkbox"><label for="checkbox12">Checkbox 1</label>
        <input id="checkbox22" type="checkbox"><label for="checkbox22">Checkbox 2</label>
        <input id="checkbox32" type="checkbox"><label for="checkbox32">Checkbox 3</label>
    </fieldset>

    <!--Help text-->
    <label>Password
        <input type="password" aria-describedby="passwordHelpText">
    </label>
    <p class="help-text" id="passwordHelpText">Your password must have at least 10 characters, a number, and an
        Emoji.</p>


    <!--Label positioning-->
    <form>
        <div class="grid-x grid-padding-x">
            <div class="small-3 cell">
                <label for="right-label" class="text-right">Label</label>
            </div>
            <div class="small-9 cell">
                <input type="text" id="right-label" placeholder="Right-aligned text input">
            </div>
        </div>
    </form>


    <!--Input-group-->
    <div class="input-group">
        <span class="input-group-label">$</span>
        <input title="" class="input-group-field" type="number">
        <div class="input-group-button">
            <input type="submit" class="button" value="Submit">
        </div>
    </div>

    <!--File upload button-->
    <label for="exampleFileUpload" class="button">Upload File</label>
    <input type="file" id="exampleFileUpload" class="show-for-sr">



<!--Cell closers-->
        </div>
    </div>
</div>
