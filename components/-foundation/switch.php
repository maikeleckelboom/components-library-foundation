<div class="grid-x master animated fadeIn">
    <div class="cell small-24 medium-11 overflow">
        <div class="cell-margin">

            <!--Default switch-->
            <div class="switch">
                <input class="switch-input" id="exampleSwitch" type="checkbox" name="exampleSwitch">
                <label class="switch-paddle" for="exampleSwitch">
                    <span class="show-for-sr">Download Kittens</span>
                </label>
            </div>

            <!--Radio Switch-->
            <div class="switch">
                <input class="switch-input" id="exampleRadioSwitch1" type="radio" checked name="testGroup">
                <label class="switch-paddle" for="exampleRadioSwitch1">
                    <span class="show-for-sr">Bulbasaur</span>
                </label>
            </div>

            <!--Switch with sizing classes-->
            <div class="switch tiny">
                <input class="switch-input" id="tinySwitch" type="checkbox" name="exampleSwitch">
                <label class="switch-paddle" for="tinySwitch">
                    <span class="show-for-sr">Tiny Sandwiches Enabled</span>
                </label>
            </div>

            <div class="switch small">
                <input class="switch-input" id="smallSwitch" type="checkbox" name="exampleSwitch">
                <label class="switch-paddle" for="smallSwitch">
                    <span class="show-for-sr">Small Portions Only</span>
                </label>
            </div>

            <div class="switch large">
                <input class="switch-input" id="largeSwitch" type="checkbox" name="exampleSwitch">
                <label class="switch-paddle" for="largeSwitch">
                    <span class="show-for-sr">Show Large Elephants</span>
                </label>
            </div>

            <!--Inner labels-->
            <div class="switch large">
                <input class="switch-input" id="yes-no" type="checkbox" name="exampleSwitch">
                <label class="switch-paddle" for="yes-no">
                    <span class="switch-active" aria-hidden="true">Yes</span>
                    <span class="switch-inactive" aria-hidden="true">No</span>
                </label>
            </div>

        </div>
    </div>


    <div class="cell small-24 medium-13 overflow">
        <div class="cell-margin">

            <h1 class="title">Switch</h1>

            <!--HTMl5-->
            <div class="component-buttons">
                <button type="button" class="flip flip-switch-html show-code-html"><i class="fab fa-html5"></i> Html</button>
<!--                <button type="button" onClick="CopyToClipboard('to-copy')" id="copied">-->
<!--                    <i class="far fa-copy"> Copy</i></button>-->
            </div>


            <!--Panel HTML5-->
            <div class="panel panel-switch-html" id="to-copy">
                <pre class="line-numbers language-markup">
                    <code id="to-copy">
                        &lt;!--Default switch--&gt;
                        &lt;div class=&quot;switch&quot;&gt;
                            &lt;input class=&quot;switch-input&quot; id=&quot;exampleSwitch&quot; type=&quot;checkbox&quot; name=&quot;exampleSwitch&quot;&gt;
                            &lt;label class=&quot;switch-paddle&quot; for=&quot;exampleSwitch&quot;&gt;
                                &lt;span class=&quot;show-for-sr&quot;&gt;Download Kittens&lt;/span&gt;
                            &lt;/label&gt;
                        &lt;/div&gt;

                        &lt;!--Radio Switch--&gt;
                        &lt;div class=&quot;switch&quot;&gt;
                            &lt;input class=&quot;switch-input&quot; id=&quot;exampleRadioSwitch1&quot; type=&quot;radio&quot; checked name=&quot;testGroup&quot;&gt;
                            &lt;label class=&quot;switch-paddle&quot; for=&quot;exampleRadioSwitch1&quot;&gt;
                                &lt;span class=&quot;show-for-sr&quot;&gt;Bulbasaur&lt;/span&gt;
                            &lt;/label&gt;
                        &lt;/div&gt;

                        &lt;!--Switch with sizing classes--&gt;
                        &lt;div class=&quot;switch tiny&quot;&gt;
                            &lt;input class=&quot;switch-input&quot; id=&quot;tinySwitch&quot; type=&quot;checkbox&quot; name=&quot;exampleSwitch&quot;&gt;
                            &lt;label class=&quot;switch-paddle&quot; for=&quot;tinySwitch&quot;&gt;
                                &lt;span class=&quot;show-for-sr&quot;&gt;Tiny Sandwiches Enabled&lt;/span&gt;
                            &lt;/label&gt;
                        &lt;/div&gt;

                        &lt;div class=&quot;switch small&quot;&gt;
                            &lt;input class=&quot;switch-input&quot; id=&quot;smallSwitch&quot; type=&quot;checkbox&quot; name=&quot;exampleSwitch&quot;&gt;
                            &lt;label class=&quot;switch-paddle&quot; for=&quot;smallSwitch&quot;&gt;
                                &lt;span class=&quot;show-for-sr&quot;&gt;Small Portions Only&lt;/span&gt;
                            &lt;/label&gt;
                        &lt;/div&gt;

                        &lt;div class=&quot;switch large&quot;&gt;
                            &lt;input class=&quot;switch-input&quot; id=&quot;largeSwitch&quot; type=&quot;checkbox&quot; name=&quot;exampleSwitch&quot;&gt;
                            &lt;label class=&quot;switch-paddle&quot; for=&quot;largeSwitch&quot;&gt;
                                &lt;span class=&quot;show-for-sr&quot;&gt;Show Large Elephants&lt;/span&gt;
                            &lt;/label&gt;
                        &lt;/div&gt;

                        &lt;!--Inner labels--&gt;
                        &lt;div class=&quot;switch large&quot;&gt;
                            &lt;input class=&quot;switch-input&quot; id=&quot;yes-no&quot; type=&quot;checkbox&quot; name=&quot;exampleSwitch&quot;&gt;
                            &lt;label class=&quot;switch-paddle&quot; for=&quot;yes-no&quot;&gt;
                                &lt;span class=&quot;switch-active&quot; aria-hidden=&quot;true&quot;&gt;Yes&lt;/span&gt;
                                &lt;span class=&quot;switch-inactive&quot; aria-hidden=&quot;true&quot;&gt;No&lt;/span&gt;
                            &lt;/label&gt;
                        &lt;/div&gt;
                    </code><button class="copy-button">Copy</button>
                </pre>
            </div>

        </div>
    </div>
</div>


<script>
    Prism.highlightAll();
    CallCopyToClipboard();

    $(document).ready(function () {
        $(".flip-switch-html").click(function () {
            $(".panel-switch-html").slideToggle("slow");
        });
    });
</script>
